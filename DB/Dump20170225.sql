CREATE DATABASE  IF NOT EXISTS `personal_diary` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `personal_diary`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: personal_diary
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorization`
--

DROP TABLE IF EXISTS `authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `datetime` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `succesfull` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `authorization_person_id_fk` (`user_id`),
  CONSTRAINT `authorization_person_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization`
--

LOCK TABLES `authorization` WRITE;
/*!40000 ALTER TABLE `authorization` DISABLE KEYS */;
/*!40000 ALTER TABLE `authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diary`
--

DROP TABLE IF EXISTS `diary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT 'название дневника\n',
  `background` varchar(300) DEFAULT NULL,
  `envelop` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `diary_person_id_fk` (`user_id`),
  CONSTRAINT `diary_person_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diary`
--

LOCK TABLES `diary` WRITE;
/*!40000 ALTER TABLE `diary` DISABLE KEYS */;
INSERT INTO `diary` VALUES (1,6,'innoplis',NULL,NULL),(2,7,'kazan',NULL,NULL),(3,8,'vesna',NULL,NULL);
/*!40000 ALTER TABLE `diary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diary_entry`
--

DROP TABLE IF EXISTS `diary_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diary_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diary_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `privacy_id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `text` varchar(2000) DEFAULT NULL,
  `image_link` varchar(300) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entry_entry_type_id_fk` (`type_id`),
  KEY `entry_entry_priority_id_fk` (`priority_id`),
  KEY `entry_entry_privacy_id_fk` (`privacy_id`),
  KEY `entry_diary_id_fk` (`diary_id`),
  CONSTRAINT `entry_diary_id_fk` FOREIGN KEY (`diary_id`) REFERENCES `diary` (`id`),
  CONSTRAINT `entry_entry_priority_id_fk` FOREIGN KEY (`priority_id`) REFERENCES `entry_priority` (`id`),
  CONSTRAINT `entry_entry_privacy_id_fk` FOREIGN KEY (`privacy_id`) REFERENCES `entry_privacy` (`id`),
  CONSTRAINT `entry_entry_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `entry_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Одиночная запись';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diary_entry`
--

LOCK TABLES `diary_entry` WRITE;
/*!40000 ALTER TABLE `diary_entry` DISABLE KEYS */;
INSERT INTO `diary_entry` VALUES (1,1,1,1,1,'выы','ывф',NULL,NULL,'2017-02-25 09:27:55'),(2,2,2,2,2,'ывфыв','ывфы',NULL,NULL,'2017-02-25 09:28:15'),(3,3,2,1,1,'выывыф','выв',NULL,NULL,'2017-02-25 09:28:34');
/*!40000 ALTER TABLE `diary_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entry_priority`
--

DROP TABLE IF EXISTS `entry_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entry_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_priority_priority_uindex` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entry_priority`
--

LOCK TABLES `entry_priority` WRITE;
/*!40000 ALTER TABLE `entry_priority` DISABLE KEYS */;
INSERT INTO `entry_priority` VALUES (1,'Высокий',1),(2,'Средний',2),(3,'Низкий',3);
/*!40000 ALTER TABLE `entry_priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entry_privacy`
--

DROP TABLE IF EXISTS `entry_privacy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entry_privacy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_privacy_name_uindex` (`name`),
  UNIQUE KEY `entry_privacy_level_uindex` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entry_privacy`
--

LOCK TABLES `entry_privacy` WRITE;
/*!40000 ALTER TABLE `entry_privacy` DISABLE KEYS */;
INSERT INTO `entry_privacy` VALUES (1,1,'private'),(2,2,'public');
/*!40000 ALTER TABLE `entry_privacy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entry_type`
--

DROP TABLE IF EXISTS `entry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entry_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `icon` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entry_type`
--

LOCK TABLES `entry_type` WRITE;
/*!40000 ALTER TABLE `entry_type` DISABLE KEYS */;
INSERT INTO `entry_type` VALUES (1,'Задача',NULL),(2,'Событие',NULL),(3,'Заметка',NULL);
/*!40000 ALTER TABLE `entry_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `birthday` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(45) NOT NULL,
  `password` varchar(50) NOT NULL,
  `telephone_number` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_roles_id_fk` (`role_id`),
  CONSTRAINT `user_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'Lena','Petrova','2017-02-25 09:18:50','t@1.ru','1','23213',2,0),(7,'Vasy','Lenin','2017-02-25 09:18:50','1@1.ru','1','223',2,0),(8,'Vyacheslav','Politov','2017-02-25 09:18:50','1@1.ru','2','3242',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'personal_diary'
--

--
-- Dumping routines for database 'personal_diary'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-25 12:30:06
