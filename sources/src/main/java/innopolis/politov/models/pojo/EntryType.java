package innopolis.politov.models.pojo;

/**
 * Created by General on 2/24/2017.
 */
public class EntryType extends DomainObject {
    private String name;
    private String icon;
    private Long version;

    public EntryType() {
    }

    public EntryType(Long id) {
        super(id);
    }

    public EntryType(Long id, String name, String icon) {
        super(id);
        this.name = name;
        this.icon = icon;
    }

    public EntryType(Long id, String name, String icon, Long version) {
        super(id);
        this.name = name;
        this.icon = icon;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
