package innopolis.politov.models.pojo;

import javax.persistence.Version;
import java.util.ArrayList;

/**
 * Created by General on 2/24/2017.
 */
public class Diary extends DomainObject {
    private String name;
    private ArrayList<DiaryEntry> diaryEntries = new ArrayList<>();
    private Integer  userId;
    private Long version;


    public Diary() {
    }

    public Diary(Long id) {
        super(id);
    }

    public Diary(Long id, String name, Integer user) {
        super(id);
        this.name = name;
        this.userId = user;
    }

    public Diary(Long id, String name, Integer userId, Long
            version) {
        super(id);
        this.name = name;
        this.userId = userId;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DiaryEntry> getDiaryEntries() {
        return diaryEntries;
    }

    public void setDiaryEntries(ArrayList<DiaryEntry> diaryEntries) {
        this.diaryEntries = diaryEntries;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer user) {
        this.userId = user;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
