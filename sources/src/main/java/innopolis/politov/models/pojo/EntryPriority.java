package innopolis.politov.models.pojo;

/**
 * Created by General on 2/24/2017.
 */
public class EntryPriority extends DomainObject {
    private  String name;
    private  Integer priority;
    private  Long version;

    public EntryPriority() {
    }

    public EntryPriority(Long id) {
        super(id);
    }

    public EntryPriority(Long id, String name, Integer priority) {
        super(id);
        this.name = name;
        this.priority = priority;
    }

    public EntryPriority(Long id, String name, Integer priority, Long version) {
        super(id);
        this.name = name;
        this.priority = priority;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
