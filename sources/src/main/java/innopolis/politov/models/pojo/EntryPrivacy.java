package innopolis.politov.models.pojo;

/**
 * Created by General on 2/24/2017.
 */
public class EntryPrivacy extends DomainObject {
    private Integer level;
    private String name;
    private Long version;

    public EntryPrivacy() {
    }

    public EntryPrivacy(Long id) {
        super(id);
    }

    public EntryPrivacy(Long id, Integer level, String name) {
        super(id);
        this.level = level;
        this.name = name;
    }

    public EntryPrivacy(Long id, Integer level, String name, Long version) {
        super(id);
        this.level = level;
        this.name = name;
        this.version = version;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
