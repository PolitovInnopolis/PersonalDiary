package innopolis.politov.models.pojo;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by General on 2/19/2017.
 */
public class DomainObject {

    private Long id = new Long(-1);

    public DomainObject() {
    }

    public DomainObject(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @XmlElement
    public void setId(Long id) {
        this.id = id;
    }
}
