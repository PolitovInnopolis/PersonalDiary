package innopolis.politov.models.pojo;

import javax.xml.bind.annotation.XmlType;

/**
 * Created by General on 2/19/2017.
 */

@XmlType (propOrder = { "name"}, name = "Role")
public final class Role extends DomainObject{
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_USER = "ROLE_USER";
    private String name;
    private Long version;

    public Role() {
    }

    public Role(Long id, String name) {
        super(id);
        this.name = name;
    }

    public Role(Long id, String name, Long version) {
        super(id);
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isAdmin(){
        return getName().toLowerCase().equals(ROLE_ADMIN.toLowerCase());
    }

    @Override
    public String toString() {
        return "Role< id= " + getId() + ", name='" + name + '\'' + '>';
    }

}
