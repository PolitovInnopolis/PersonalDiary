package innopolis.politov.models.pojo;

import java.sql.Timestamp;

/**
 * Created by General on 2/24/2017.
 */
public class DiaryEntry extends DomainObject {
    private Long diaryId;
    private EntryPriority entryPriority;
    private EntryPrivacy entryPrivacy;
    private EntryType entryType;
    private String title;
    private String text;
    private String imageLink;
    private String tag;
    private Timestamp dateTime;
    private Long version;

    public DiaryEntry() {
    }

    public DiaryEntry(Long id) {
        super(id);
    }

    public DiaryEntry(Long id, Long diaryId, EntryPriority entryPriority,
                      EntryPrivacy entryPrivacy, EntryType entryType, String
                              title, String text, String imageLink, String tag, String dateTime) {
        super(id);
        this.diaryId = diaryId;
        this.entryPriority = entryPriority;
        this.entryPrivacy = entryPrivacy;
        this.entryType = entryType;
        this.title = title;
        this.text = text;
        this.imageLink = imageLink;
        this.tag = tag;
        this.dateTime = Timestamp.valueOf(dateTime);
    }

    public DiaryEntry(Long id, Long diaryId, EntryPriority entryPriority,
                      EntryPrivacy entryPrivacy, EntryType entryType, String
                              title, String text, String imageLink, String tag, Timestamp dateTime,
                      Long version) {
        super(id);
        this.diaryId = diaryId;
        this.entryPriority = entryPriority;
        this.entryPrivacy = entryPrivacy;
        this.entryType = entryType;
        this.title = title;
        this.text = text;
        this.imageLink = imageLink;
        this.tag = tag;
        this.dateTime = dateTime;
        this.version = version;
    }

    public Long getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Long diary) {
        this.diaryId = diary;
    }

    public EntryPriority getEntryPriority() {
        return entryPriority;
    }

    public void setEntryPriority(EntryPriority entryPriority) {
        this.entryPriority = entryPriority;
    }

    public EntryPrivacy getEntryPrivacy() {
        return entryPrivacy;
    }

    public void setEntryPrivacy(EntryPrivacy entryPrivacy) {
        this.entryPrivacy = entryPrivacy;
    }

    public EntryType getEntryType() {
        return entryType;
    }

    public void setEntryType(EntryType entryType) {
        this.entryType = entryType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }
}
