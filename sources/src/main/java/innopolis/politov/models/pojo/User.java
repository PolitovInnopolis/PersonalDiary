package innopolis.politov.models.pojo;

import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by General on 2/19/2017.
 */
@XmlType(propOrder = { "firstName", "lastName", "birthDay",
        "email", "password", "telephoneNumber", "role", "unBlocked"
        , "diaries"}, name =
        "User")
public class User extends DomainObject {

    private String firstName;
    private String lastName;
    private Date birthDay;
    private String email;
    private String password;
    private String telephoneNumber;
    private Role role;
    private Boolean unBlocked;
    private ArrayList<Diary> diaries = new ArrayList<>();
    private Long version;

    public User() {
    }

    public User(Long id) {
        super(id);
    }

    public User(User user){
        this.setId(user.getId());
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.birthDay = user.birthDay;
        this.diaries =user.diaries;
        this.email = user.email;
        this.password = user.password;
        this.role = user.role;
        this.unBlocked = user.unBlocked;
        this.telephoneNumber = user.telephoneNumber;
    }




    public User(Long id, String firstName, String lastName, Date birthDay, String email, String
            password, String telephoneNumber, Role role, Boolean unBlocked) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.email = email;
        this.password = password;
        this.telephoneNumber = telephoneNumber;
        this.role = role;
        this.unBlocked = unBlocked;
    }

    public User(Long id, String firstName, String lastName, Date birthDay, String email, String
            password, String telephoneNumber, Role role, Boolean unBlocked, ArrayList<Diary> diaries) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.email = email;
        this.password = password;
        this.telephoneNumber = telephoneNumber;
        this.role = role;
        this.unBlocked = unBlocked;
        this.diaries = diaries;
    }

    public User(Long id, Long version, String firstName, String lastName, Date birthDay, String
            email, String password, String telephoneNumber, Role role, Boolean unBlocked,
                ArrayList<Diary> diaries) {
        super(id);
        this.version = version;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.email = email;
        this.password = password;
        this.telephoneNumber = telephoneNumber;
        this.role = role;
        this.unBlocked = unBlocked;
        this.diaries = diaries;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getUnBlocked() {
        return unBlocked;
    }

    public void setUnBlocked(Boolean unBlocked) {
        this.unBlocked = unBlocked;
    }

    public ArrayList<Diary> getDiaries() {
        return diaries;
    }

    public void setDiaries(ArrayList<Diary> diaries) {
        this.diaries = diaries;
    }

    @Override
    public String toString() {
        return "User< id = " + getId() +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDay=" + birthDay +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", role=" + role +
                ", unBlocked=" + unBlocked +
                '>';
    }
}
