package innopolis.politov.models.converters;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.entity.DiaryEntryEntity;
import innopolis.politov.models.pojo.DiaryEntry;
import innopolis.politov.models.pojo.EntryPriority;
import innopolis.politov.models.pojo.EntryPrivacy;
import innopolis.politov.models.pojo.EntryType;
import innopolis.politov.models.repositories.EntryPriorityRepository;
import innopolis.politov.models.repositories.EntryPrivacyRepository;
import innopolis.politov.models.repositories.EntryTypeRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by General on 3/20/2017.
 */
@Component
public class DiaryEntryConverter {
    public static final Logger logger = Logger.getLogger(DiaryEntryConverter.class);

    private EntryPriorityRepository entryPriorityRepository;
    private EntryTypeRepository entryTypeRepository;
    private EntryPrivacyRepository entryPrivacyRepository;

    /**
     * Преобразует entity hibernate в объект pojo
     * @param entity
     * @return
     */
    public DiaryEntry convert(DiaryEntryEntity entity) {
        logger.trace("Diary entry convert start");
        EntryPriority entryPriority = EntryPriorityConverter.convert(entryPriorityRepository.findByIdAndActive(entity
                .getPriorityId(), ConstEntityActive.ENTITY_ACTIVE));
        EntryType entryType = EntryTypeConverter.convert( entryTypeRepository.findByIdAndActive(entity
                .getTypeId(), ConstEntityActive.ENTITY_ACTIVE));
        EntryPrivacy entryPrivacy = EntryPrivacyConverter.convert(entryPrivacyRepository
                .findByIdAndActive(entity.getPrivacyId(), ConstEntityActive.ENTITY_ACTIVE));

        DiaryEntry diaryEntry = new DiaryEntry(
                Long.valueOf(entity.getId()),
                Long.valueOf(entity.getDiaryId()),
                entryPriority,
                entryPrivacy,
                entryType,
                entity.getTitle(),
                entity.getText(),
                entity.getImageLink(),
                entity.getTag(),
                entity.getDateTime(),
                entity.getVersion().longValue()
        );
        logger.trace("Diary entry convert end");
        return diaryEntry;
    }

    public  List<DiaryEntry> convert(List<DiaryEntryEntity> entity)  {
        ArrayList<DiaryEntry> entries = new ArrayList<>();
        for (DiaryEntryEntity entryEntity :
                entity) {
           logger.trace("Convert entry entity " + entryEntity);
            entries.add(convert(entryEntity));
        }
        return entries;
    }


    @Autowired
    public void setEntryPriorityRepository(EntryPriorityRepository entryPriorityRepository) {
        this.entryPriorityRepository = entryPriorityRepository;
    }


    @Autowired
    public void setEntryTypeRepository(EntryTypeRepository entryTypeRepository) {
        this.entryTypeRepository = entryTypeRepository;
    }

    @Autowired
    public void setEntryPrivacyRepository(EntryPrivacyRepository entryPrivacyRepository) {
        this.entryPrivacyRepository = entryPrivacyRepository;
    }
}
