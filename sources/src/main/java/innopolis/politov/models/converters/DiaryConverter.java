package innopolis.politov.models.converters;

import innopolis.politov.models.entity.DiaryEntity;
import innopolis.politov.models.pojo.Diary;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by General on 3/19/2017.
 */
public class DiaryConverter {
    private static final Logger logger = Logger.getLogger(DiaryConverter.class);

    public static Diary convert(DiaryEntity entity){
        return new Diary(
                Long.valueOf(entity.getId()),
                entity.getName(),
                entity.getUserId(),
                entity.getVersion().longValue());
    }

    public static List<Diary> convert(List<DiaryEntity> entity) {
        logger.trace("CONVERT LIST OF DIARY ENTITY");
        ArrayList<Diary> diaries = new ArrayList<>();
        for (DiaryEntity diaryEntity :
                entity) {
            diaries.add(convert(diaryEntity));
        }
        logger.trace("CONVERT LIST OF DIARY ENTITY - END");
        return diaries;
    }
}
