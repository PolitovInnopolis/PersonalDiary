package innopolis.politov.models.converters;

import innopolis.politov.models.entity.RolesEntity;
import innopolis.politov.models.pojo.Role;
import org.apache.log4j.Logger;

/**
 * Created by General on 3/19/2017.
 */
public class RoleConverter {
    public static final Logger logger = Logger.getLogger(RoleConverter.class);

    public static Role convert(RolesEntity entity){
        logger.trace("Role convert start");
        Role role = new Role(
                (long) entity.getId(),
                entity.getName(),
                entity.getVersion().longValue());
        logger.trace("Role convert end");
        return role;
    }
}
