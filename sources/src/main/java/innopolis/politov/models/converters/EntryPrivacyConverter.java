package innopolis.politov.models.converters;

import innopolis.politov.models.entity.EntryPrivacyEntity;
import innopolis.politov.models.pojo.EntryPrivacy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by General on 3/20/2017.
 */
public class EntryPrivacyConverter {


    public static EntryPrivacy convert(EntryPrivacyEntity entity){
        return new EntryPrivacy(
                Long.valueOf(entity.getId()),
                entity.getLevel(),
                entity.getName(),
                entity.getVersion().longValue()
        );
    }

    public static List<EntryPrivacy> convert(List<EntryPrivacyEntity> entity) {
        ArrayList<EntryPrivacy> entryPrivacies = new ArrayList<>();
        for (EntryPrivacyEntity privacyEntity :
                entity) {
            entryPrivacies.add(convert(privacyEntity));
        }
        return entryPrivacies;
    }
}
