package innopolis.politov.models.converters;

import innopolis.politov.models.entity.EntryPriorityEntity;
import innopolis.politov.models.pojo.EntryPriority;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by General on 3/20/2017.
 */
public class EntryPriorityConverter {

    public static EntryPriority convert(EntryPriorityEntity entity){
        return new EntryPriority(
                Long.valueOf(entity.getId()),
                entity.getName(),
                entity.getPriority(),
                entity.getVersion().longValue()
        );
    }

    public static List<EntryPriority> convert(List<EntryPriorityEntity> entity) {
        ArrayList<EntryPriority> priorities = new ArrayList<>();
        for (EntryPriorityEntity priorityEntity :
                entity) {
            priorities.add(convert(priorityEntity));
        }
        return priorities;
    }
}
