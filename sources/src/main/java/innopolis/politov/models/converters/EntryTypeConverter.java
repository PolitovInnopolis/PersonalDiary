package innopolis.politov.models.converters;

import innopolis.politov.models.entity.EntryTypeEntity;
import innopolis.politov.models.pojo.EntryPriority;
import innopolis.politov.models.pojo.EntryType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by General on 3/20/2017.
 */
public class EntryTypeConverter {

    public static EntryType convert(EntryTypeEntity entity){
        return new EntryType(
                Long.valueOf(entity.getId()),
                entity.getName(),
                entity.getIcon(),
                entity.getVersion().longValue()
        );
    }

    public static List<EntryType> convert(List<EntryTypeEntity> entity) {
        ArrayList<EntryType> entryTypes = new ArrayList<>();
        for (EntryTypeEntity typeEntity :
                entity) {
            entryTypes.add(convert(typeEntity));
        }
        return entryTypes;
    }
}
