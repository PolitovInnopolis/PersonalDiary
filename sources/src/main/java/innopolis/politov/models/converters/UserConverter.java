package innopolis.politov.models.converters;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.entity.UsersEntity;
import innopolis.politov.models.pojo.Diary;
import innopolis.politov.models.pojo.Role;
import innopolis.politov.models.pojo.User;
import innopolis.politov.models.repositories.DiaryRepository;
import innopolis.politov.models.repositories.RoleRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by General on 3/19/2017.
 */
@Component
public class UserConverter {
    public static final Logger logger = Logger.getLogger(UserConverter.class);
    private DiaryRepository diaryRepository;

    private RoleRepository roleRepository;

    public User convert(UsersEntity entity){
        List<Diary> diaries = DiaryConverter.convert(diaryRepository.findByUserIdAndActive(entity.getId(),
                ConstEntityActive.ENTITY_ACTIVE));
        Role role = RoleConverter.convert(roleRepository.findOne(entity.getRoleId()));

        User user = new User(
                (long) entity.getId(),
                entity.getVersion().longValue(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getBirthday(),
                entity.getEmail(),
                entity.getPassword(),
                entity.getTelephoneNumber(),
                role,
                entity.getBlocked()>0 ? true : false,
                (ArrayList<Diary>) diaries
        );
        logger.trace("CONVERT END");
        return user;

    }

    public  List<User> convert(List<UsersEntity> entity) {
        ArrayList<User> users = new ArrayList<>();
        for (UsersEntity usersEntity :
                entity) {
            users.add(convert(usersEntity));
        }
        return users;
    }

    @Autowired
    public void setDiaryRepository(DiaryRepository diaryRepository) {
        this.diaryRepository = diaryRepository;
    }

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
}
