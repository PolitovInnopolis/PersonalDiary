package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.DiaryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by General on 3/23/2017.
 */
public interface DiaryRepository extends CrudRepository<DiaryEntity, Integer> {
    List<DiaryEntity> findByUserIdAndActive(Integer userId, Integer active);
}
