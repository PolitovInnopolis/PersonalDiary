package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.RolesEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by General on 3/23/2017.
 */
public interface RoleRepository extends CrudRepository<RolesEntity, Integer> {
    RolesEntity findByName(String name);
}
