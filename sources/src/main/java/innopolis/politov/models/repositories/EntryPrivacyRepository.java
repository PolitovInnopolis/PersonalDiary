package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.EntryPrivacyEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by General on 3/23/2017.
 */
public interface EntryPrivacyRepository extends CrudRepository<EntryPrivacyEntity, Integer> {
    List<EntryPrivacyEntity> findByActive(Integer active);
    EntryPrivacyEntity findByIdAndActive(Integer id, Integer active);
}
