package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.DiaryEntryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by General on 3/28/2017.
 */
public interface DiaryEntryRepository extends CrudRepository<DiaryEntryEntity, Integer> {
    DiaryEntryEntity findByIdAndActive(Integer id, Integer active);
    List<DiaryEntryEntity> findByDiaryIdAndActive(Integer id, Integer active);
}
