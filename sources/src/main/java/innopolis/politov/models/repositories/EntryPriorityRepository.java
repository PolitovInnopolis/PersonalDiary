package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.EntryPriorityEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by General on 3/23/2017.
 */
public interface EntryPriorityRepository extends CrudRepository<EntryPriorityEntity, Integer> {
    List<EntryPriorityEntity> findByActive(Integer active);
    EntryPriorityEntity findByIdAndActive(Integer id, Integer active);
}
