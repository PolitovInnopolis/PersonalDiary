package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.UsersEntity;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by General on 3/23/2017.
 */
public interface UserRepository extends CrudRepository<UsersEntity, Integer> {

    UsersEntity findByEmailAndActive(String email, Integer active);

    List<UsersEntity> findByActive(Integer active);

    UsersEntity findByEmailAndPassword(String login, String password);
}
