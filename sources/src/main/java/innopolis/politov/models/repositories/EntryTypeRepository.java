package innopolis.politov.models.repositories;

import innopolis.politov.models.entity.EntryTypeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by General on 3/23/2017.
 */
public interface EntryTypeRepository extends CrudRepository<EntryTypeEntity, Integer> {
    List<EntryTypeEntity> findByActive(Integer active);
    EntryTypeEntity findByIdAndActive(Integer id, Integer active);
}
