package innopolis.politov.models.database;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by General on 2/20/2017.
 */
public class DataBase {
    public static final Logger logger = Logger.getLogger(DataBase.class);
    static
    {
        BasicConfigurator.configure();
    }
        static {
        DOMConfigurator.configure
                ("/logger.xml");
    }

    private static final String driver = "com.mysql.jdbc.Driver";

    private static final String url =
            "jdbc:mysql://localhost:3306/personal_diary" +
            "?useUnicode=true&useSSL=false&useJDBCCompliantTimezoneShift=true" +
            "&useLegacyDatetimeCode=false&serverTimezone=UTC";
    //"jdbc:mysql://localhost:3306/personal_diary?useSSL
    // =false";

    private static final String login = "root";
    private static final String password = "root";



    private Connection connection = null;

    private static volatile DataBase instance;

    private DataBase() {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, login, password);
        }
        catch (ClassNotFoundException e) {
            logger.error(e);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static DataBase getInstance() {
            DataBase localInstance = instance;
            if (localInstance == null) {
                synchronized (DataBase.class) {
                    localInstance = instance;
                    if (localInstance == null) {
                        instance = localInstance = new DataBase();
                    }
                }
            }
            return localInstance;
        }

    public Connection getConnection() {
        return connection;
    }
}
