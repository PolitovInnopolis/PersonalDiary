package innopolis.politov.models.database;

import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by General on 2/22/2017.
 */
public class XmlDataManager {
    public static final Logger logger = Logger.getLogger(XmlDataManager.class);
    public static void serialize(String fileName,  Object obj){
        try {

            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(obj, file);
//            jaxbMarshaller.marshal(obj, System.out);

        } catch (JAXBException e) {
            logger.error("Ошибка сериализации ", e);
        }
    }

    public static Object desrialize(String fileName, Class objectClass ){
        Object result = null;
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance( objectClass);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result =  jaxbUnmarshaller.unmarshal(file);
            System.out.println(result);

        } catch (JAXBException e) {
            logger.error("Не удалось десериализовать объект", e);
        }
        return result;
    }

}
