package innopolis.politov.models.database;

import innopolis.politov.models.dao.*;
import innopolis.politov.models.dao.edao.DaoGeneralException;
import innopolis.politov.models.dao.edao.UserDaoException;
import org.apache.log4j.Logger;


import java.util.concurrent.*;

/**
 * Created by General on 2/25/2017.
 */
public class DataBaseManager {
    public static final Logger logger = Logger.getLogger(DataBaseManager.class);
    /**
     * Экспорт всех таблиц БД (в многопоточном режиме) в xml файлы, каждая
     * таблица в свой файл
     */
    public static void ExportDataBaseToXml(){
//        logger.trace("Начат экспорт БД в xml");
//        final RoleDao roleDao = new RoleDao();
//        final UserDao userDao = new UserDao();
//        final EntryPriorityDao entryPriorityDao = new EntryPriorityDao();
//        final EntryPrivacyDao entryPrivacyDao = new EntryPrivacyDao();
//        final EntryTypeDao entryTypeDao = new EntryTypeDao();
//        final DiaryEntryDao diaryEntryDao  = new DiaryEntryDao();
//        final DiaryDao diaryDao = new DiaryDao();
//
//            ExecutorService service = Executors.newFixedThreadPool(7, Executors.defaultThreadFactory());
//
//            service.submit(() -> {
//                try {
//                    userDao.loadAllElementTable();
//                } catch (DaoGeneralException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.submit(() -> {
//                try {
//                    roleDao.loadAllElementTable();
//                } catch (DaoGeneralException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.submit(() -> {
//                try {
//                    diaryDao.loadAllElementTable();
//                } catch (UserDaoException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.submit(() -> {
//                try {
//                    diaryEntryDao.loadAllElementTable();
//                } catch (DaoGeneralException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.submit(() -> {
//                try {
//                    entryTypeDao.loadAllElementTable();
//                } catch (DaoGeneralException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.submit(() -> {
//                try {
//                    entryPriorityDao.loadAllElementTable();
//                } catch (DaoGeneralException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.submit(() -> {
//                try {
//                    entryPrivacyDao.loadAllElementTable();
//                } catch (DaoGeneralException e) {
//                    e.printStackTrace();
//                }
//            });
//            service.shutdown();
//            while (!service.isTerminated()) ;


//        //jaxb serialize
//        XmlDataManager.serialize("tmp\\RoleDao.xml", roleDao);
//        XmlDataManager.serialize("tmp\\UserDao.xml", userDao);
//        XmlDataManager.serialize("tmp\\entryTypeDao.xml", entryTypeDao);
//        XmlDataManager.serialize("tmp\\entryPriorityDao.xml", entryPriorityDao);
//        XmlDataManager.serialize("tmp\\entryPrivacyDao.xml", entryPrivacyDao);
//        XmlDataManager.serialize("tmp\\diaryDao.xml", diaryDao);
//        XmlDataManager.serialize("tmp\\diaryEntryDao.xml", diaryEntryDao);
//
//        logger.trace("Экспорт БД в xml завершен");
    }

    /**
     * Очистка всех записей во всех таблиц БД
    */
    public static void ClearAllTableInDb(){
//        logger.trace("Очистка таблиц БД начата");
//        final RoleDao roleDao = new RoleDao();
//        final UserDao userDao = new UserDao();
//        final EntryPriorityDao entryPriorityDao = new EntryPriorityDao();
//        final EntryPrivacyDao entryPrivacyDao = new EntryPrivacyDao();
//        final EntryTypeDao entryTypeDao = new EntryTypeDao();
//        final DiaryEntryDao diaryEntryDao  = new DiaryEntryDao();
//        final DiaryDao diaryDao = new DiaryDao();
//
//        try {
//            diaryEntryDao.deleteAllElementTable();
//            entryTypeDao.deleteAllElementTable();
//            entryPriorityDao.deleteAllElementTable();
//            entryPrivacyDao.deleteAllElementTable();
//            diaryDao.deleteAllElementTable();
//            userDao.deleteAllElementTable();
//            roleDao.deleteAllElementTable();
//        } catch (DaoGeneralException e) {
//            logger.error(e);
//        }
//
//        try {
//            Thread.sleep(12000);
//        } catch (InterruptedException e) {
//            logger.error(e);
//        }
//        logger.trace("Очистка таблиц БД завершена");

    }

    /**
     Импорт всех таблиц БД (в многопоточном режиме) из xml файла, каждая
     * таблица из свой файла, созданного в
     * DataBaseManager.ExportDataBaseToXml()
     */
    public static void ImportDataBaseFromXml(){
//        logger.trace("Импорт таблиц БД начат");
//        //deserialize
//        final RoleDao roleDao1 = (RoleDao) XmlDataManager.desrialize
//                ("tmp\\RoleDao.xml",
//                RoleDao.class);
//        final UserDao userDao1 = (UserDao) XmlDataManager.desrialize
//                ("tmp\\UserDao" +
//                ".xml", UserDao.class);
//        final EntryTypeDao entryTypeDao1 = (EntryTypeDao)XmlDataManager
//                .desrialize
//                ("tmp\\entryTypeDao.xml", EntryTypeDao.class);
//        final EntryPriorityDao entryPriorityDao1 = (EntryPriorityDao)
//                XmlDataManager.desrialize
//                ("tmp\\entryPriorityDao.xml", EntryPriorityDao.class);
//
//        final EntryPrivacyDao entryPrivacyDao1 = (EntryPrivacyDao)
//                XmlDataManager.desrialize
//                ("tmp\\entryPrivacyDao.xml", EntryPrivacyDao.class);
//
//        final DiaryDao diaryDao1 = (DiaryDao) XmlDataManager.desrialize
//                ("tmp\\diaryDao" +
//                ".xml", DiaryDao.class);
//
//        final DiaryEntryDao diaryEntryDao1 = (DiaryEntryDao) XmlDataManager
//                .desrialize
//                ("tmp\\diaryEntryDao.xml", DiaryEntryDao.class);
//
//        //import
//        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
//        cachedThreadPool.submit(() -> {
//            try {
//                userDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.submit(() -> {
//            try {
//                roleDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.submit(() -> {
//            try {
//                diaryDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.submit(() -> {
//            try {
//                diaryEntryDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.submit(() -> {
//            try {
//                entryTypeDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.submit(() -> {
//            try {
//                entryPriorityDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.submit(() -> {
//            try {
//                entryPrivacyDao1.writeAllElementToTable();
//            } catch (DaoGeneralException e) {
//                e.printStackTrace();
//            }
//        });
//        cachedThreadPool.shutdown();
//
//        while( ! cachedThreadPool.isTerminated());
//        logger.trace("Импорт таблиц БД завершен");
    }
}
