package innopolis.politov.models.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by General on 3/19/2017.
 */
@Entity
@Table(name = "entry_privacy", schema = "personal_diary", catalog = "")
public class EntryPrivacyEntity {
    private int id;
    private int level;
    private String name;
    private Integer version;
    private Integer active;
    private Collection<DiaryEntryEntity> diaryEntriesById;

    public EntryPrivacyEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "level", nullable = false)
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Version
    @Column(name = "version", nullable = false)
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntryPrivacyEntity that = (EntryPrivacyEntity) o;

        if (id != that.id) return false;
        if (level != that.level) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;
        if (active != null ? !active.equals(that.active) : that.active != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + level;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "entryPrivacyByPrivacyId", fetch = FetchType.LAZY)
    public Collection<DiaryEntryEntity> getDiaryEntriesById() {
        return diaryEntriesById;
    }

    public void setDiaryEntriesById(Collection<DiaryEntryEntity> diaryEntriesById) {
        this.diaryEntriesById = diaryEntriesById;
    }
}
