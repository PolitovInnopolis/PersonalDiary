package innopolis.politov.models.entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by General on 3/19/2017.
 */
@Entity
@Table(name = "entry_priority", schema = "personal_diary", catalog = "")
public class EntryPriorityEntity {
    private int id;
    private String name;
    private int priority;
    private Integer version;
    private Integer active;
    private Collection<DiaryEntryEntity> diaryEntriesById;

    public EntryPriorityEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "priority", nullable = false)
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Version
    @Column(name = "version", nullable = false)
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntryPriorityEntity that = (EntryPriorityEntity) o;

        if (id != that.id) return false;
        if (priority != that.priority) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;
        if (active != null ? !active.equals(that.active) : that.active != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + priority;
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "entryPriorityByPriorityId", fetch = FetchType.LAZY)
    public Collection<DiaryEntryEntity> getDiaryEntriesById() {
        return diaryEntriesById;
    }

    public void setDiaryEntriesById(Collection<DiaryEntryEntity> diaryEntriesById) {
        this.diaryEntriesById = diaryEntriesById;
    }
}
