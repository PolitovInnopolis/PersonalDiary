package innopolis.politov.models.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by General on 3/19/2017.
 */
@Entity
@Table(name = "diary_entry", schema = "personal_diary", catalog = "")
public class DiaryEntryEntity {
    private int id;
    private Integer diaryId;
    private int typeId;
    private int priorityId;
    private int privacyId;
    private String title;
    private String text;
    private String tag;
    private Timestamp dateTime;
    private String imageLink;
    private Integer version;
    private Integer active;
    private DiaryEntity diaryByDiaryId;
    private EntryTypeEntity entryTypeByTypeId;
    private EntryPriorityEntity entryPriorityByPriorityId;
    private EntryPrivacyEntity entryPrivacyByPrivacyId;

    public DiaryEntryEntity(int id, Integer diaryId, int typeId, int priorityId,
                            int privacyId, String title, String text, String tag,
                            Timestamp dateTime, String imageLink, Integer version, Integer active) {
        this.id = id;
        this.diaryId = diaryId;
        this.typeId = typeId;
        this.priorityId = priorityId;
        this.privacyId = privacyId;
        this.title = title;
        this.text = text;
        this.tag = tag;
        this.dateTime = dateTime;
        this.imageLink = imageLink;
        this.version = version;
        this.active = active;
    }

    public DiaryEntryEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "diary_id", nullable = true)
    public Integer getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Integer diaryId) {
        this.diaryId = diaryId;
    }

    @Basic
    @Column(name = "type_id", nullable = false)
    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "priority_id", nullable = false)
    public int getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(int priorityId) {
        this.priorityId = priorityId;
    }

    @Basic
    @Column(name = "privacy_id", nullable = false)
    public int getPrivacyId() {
        return privacyId;
    }

    public void setPrivacyId(int privacyId) {
        this.privacyId = privacyId;
    }

    @Basic
    @Column(name = "title", nullable = true, length = -1)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "text", nullable = true, length = -1)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "tag", nullable = true, length = -1)
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Basic
    @Column(name = "date_time", nullable = true)
    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "image_link", nullable = true, length = -1)
    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    @Version
    @Column(name = "version", nullable = false)
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiaryEntryEntity that = (DiaryEntryEntity) o;

        if (id != that.id) return false;
        if (typeId != that.typeId) return false;
        if (priorityId != that.priorityId) return false;
        if (privacyId != that.privacyId) return false;
        if (diaryId != null ? !diaryId.equals(that.diaryId) : that.diaryId != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (tag != null ? !tag.equals(that.tag) : that.tag != null) return false;
        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null)
            return false;
        if (imageLink != null ? !imageLink.equals(that.imageLink) : that.imageLink != null)
            return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;
        if (active != null ? !active.equals(that.active) : that.active != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (diaryId != null ? diaryId.hashCode() : 0);
        result = 31 * result + typeId;
        result = 31 * result + priorityId;
        result = 31 * result + privacyId;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (imageLink != null ? imageLink.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "diary_id", referencedColumnName = "id", insertable = false, updatable =
            false)
    public DiaryEntity getDiaryByDiaryId() {
        return diaryByDiaryId;
    }

    public void setDiaryByDiaryId(DiaryEntity diaryByDiaryId) {
        this.diaryByDiaryId = diaryByDiaryId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public EntryTypeEntity getEntryTypeByTypeId() {
        return entryTypeByTypeId;
    }

    public void setEntryTypeByTypeId(EntryTypeEntity entryTypeByTypeId) {
        this.entryTypeByTypeId = entryTypeByTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "priority_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public EntryPriorityEntity getEntryPriorityByPriorityId() {
        return entryPriorityByPriorityId;
    }

    public void setEntryPriorityByPriorityId(EntryPriorityEntity entryPriorityByPriorityId) {
        this.entryPriorityByPriorityId = entryPriorityByPriorityId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "privacy_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public EntryPrivacyEntity getEntryPrivacyByPrivacyId() {
        return entryPrivacyByPrivacyId;
    }

    public void setEntryPrivacyByPrivacyId(EntryPrivacyEntity entryPrivacyByPrivacyId) {
        this.entryPrivacyByPrivacyId = entryPrivacyByPrivacyId;
    }
}
