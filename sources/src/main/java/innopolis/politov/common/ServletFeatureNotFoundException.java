package innopolis.politov.common;

/**
 * Created by General on 2/28/2017.
 */
public class ServletFeatureNotFoundException extends Exception {
    public static final String ERROR_MESSAGE = "Данная функция находится в разработке. ";

    public ServletFeatureNotFoundException() {
        this("");
    }

    public ServletFeatureNotFoundException(String message) {
        super(ERROR_MESSAGE + message);
    }

    public ServletFeatureNotFoundException(String message, Throwable cause) {
        super(ERROR_MESSAGE + message, cause);
    }

    public ServletFeatureNotFoundException(Throwable cause) {
        super(cause);
    }

    public ServletFeatureNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(ERROR_MESSAGE + message, cause, enableSuppression, writableStackTrace);
    }
}
