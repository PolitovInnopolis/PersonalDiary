package innopolis.politov.common;

/**
 * Created by General on 3/2/2017.
 */
public class ExceptionUserServiceUserDataNotValid extends  Exception {
    public ExceptionUserServiceUserDataNotValid() {
        this("");
    }

    public ExceptionUserServiceUserDataNotValid(String message) {
        super("User data incorrect. " + message);
    }

    public ExceptionUserServiceUserDataNotValid(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionUserServiceUserDataNotValid(Throwable cause) {
        super(cause);
    }

    public ExceptionUserServiceUserDataNotValid(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
