package innopolis.politov.common;

/**
 * Created by General on 3/1/2017.
 */
public class ExceptionNotAccessRights extends Exception {
    public ExceptionNotAccessRights() {
        this("У Вас нет прав доступа к ресурсу или на выполнение действия");
    }

    public ExceptionNotAccessRights(String message) {
        super(message);
    }

    public ExceptionNotAccessRights(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionNotAccessRights(Throwable cause) {
        super(cause);
    }

    public ExceptionNotAccessRights(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
