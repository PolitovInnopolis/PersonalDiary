package innopolis.politov.common;

/**
 * Created by General on 3/1/2017.
 */
public class ExceptionUserDaoUserNotFound extends  Exception {
    public ExceptionUserDaoUserNotFound() {
        this("Пользователь не найден");
    }

    public ExceptionUserDaoUserNotFound(String message) {
        super(message);
    }

    public ExceptionUserDaoUserNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionUserDaoUserNotFound(Throwable cause) {
        super(cause);
    }

    public ExceptionUserDaoUserNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
