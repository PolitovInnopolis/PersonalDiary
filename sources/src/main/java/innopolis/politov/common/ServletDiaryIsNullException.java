package innopolis.politov.common;

/**
 * Created by General on 2/28/2017.
 */
public class ServletDiaryIsNullException extends Exception {
    public ServletDiaryIsNullException() {
    }

    public ServletDiaryIsNullException(String message) {
        super(message);
    }

    public ServletDiaryIsNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServletDiaryIsNullException(Throwable cause) {
        super(cause);
    }

    public ServletDiaryIsNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
