package innopolis.politov.common;

/**
 * Created by General on 2/26/2017.
 */
public class LoginSessionExcepion extends Exception {
    public LoginSessionExcepion() {
        super();
    }

    public LoginSessionExcepion(String message) {
        super(message);
    }

    public LoginSessionExcepion(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginSessionExcepion(Throwable cause) {
        super(cause);
    }

    protected LoginSessionExcepion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
