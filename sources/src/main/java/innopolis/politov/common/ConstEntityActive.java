package innopolis.politov.common;

/**
 * Created by General on 3/21/2017.
 */
public class ConstEntityActive {
    public static final Integer ENTITY_NO_ACTIVE = 0;
    public static final Integer ENTITY_ACTIVE = 1;
}
