package innopolis.politov.common;

/**
 * Created by General on 3/1/2017.
 */
public class ExceptionServletUserIdIsNull extends Exception {
    public ExceptionServletUserIdIsNull() {
        this("Параметр userId не найден");
    }

    public ExceptionServletUserIdIsNull(String message) {
        super(message);
    }

    public ExceptionServletUserIdIsNull(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionServletUserIdIsNull(Throwable cause) {
        super(cause);
    }

    public ExceptionServletUserIdIsNull(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
