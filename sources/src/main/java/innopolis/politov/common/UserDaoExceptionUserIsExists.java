package innopolis.politov.common;

/**
 * Created by General on 2/27/2017.
 */
public class UserDaoExceptionUserIsExists extends Exception {
    public UserDaoExceptionUserIsExists() {
    }

    public UserDaoExceptionUserIsExists(String message) {
        super(message);
    }

    public UserDaoExceptionUserIsExists(String message, Throwable cause) {
        super(message, cause);
    }

    public UserDaoExceptionUserIsExists(Throwable cause) {
        super(cause);
    }

    public UserDaoExceptionUserIsExists(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
