package innopolis.politov.common;

/**
 * Created by General on 3/1/2017.
 */
public class ExceptionUserBlocked  extends Exception{
    public ExceptionUserBlocked() {
        this("You ara banned!");
    }

    public ExceptionUserBlocked(String message) {
        super(message);
    }

    public ExceptionUserBlocked(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionUserBlocked(Throwable cause) {
        super(cause);
    }

    public ExceptionUserBlocked(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
