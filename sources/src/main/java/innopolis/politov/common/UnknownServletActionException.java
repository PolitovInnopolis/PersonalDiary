package innopolis.politov.common;

/**
 * Created by General on 3/3/2017.
 */
public class UnknownServletActionException extends Exception {
    private static final String ERROR_MESSAGE = "Сервлет не может выполнить указанное действие. ";

    public UnknownServletActionException() {
        this(ERROR_MESSAGE);
    }

    public UnknownServletActionException(String message) {
        super(ERROR_MESSAGE + message);
    }

    public UnknownServletActionException(String message, Throwable cause) {
        super(ERROR_MESSAGE + message, cause);
    }

    public UnknownServletActionException(Throwable cause) {
        super(cause);
    }

    public UnknownServletActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
