package innopolis.politov.common;

/**
 * Created by General on 3/7/2017.
 */
public class ConstPageParams {
    public  static class   Views {
        public static final String LOGIN = "login";
        public static final String REGISTRATION = "registration";
        public static final String MENU = "menu";
        public static final String USER_LIST = "userList";
        public static final String DIARY_LIST = "diaryList";
        public static final String DIARY_ENTRY_LIST = "diaryEntryList";
        public static final String ERROR = "error";
        public static final String DIARY_ENTRY_EDITOR = "diaryEntryEditor";
        public static final String DIARY_ENTRY_VIEWER = "diaryEntryViewer";
    }

    public static class Urls{
        public static final String REDIRECT_TO = "redirect:";

        public static final String URL_PERSONAL_DIARY = "/personaldiary";

        public static final String  URL_LOGIN = URL_PERSONAL_DIARY + "/login";
        public static final String  URL_LOGIN_SIMPLE = URL_PERSONAL_DIARY + "/";

        public static final String  URL_REGISTRATION = URL_PERSONAL_DIARY + "/registration";

        public static final String  URL_DIARY_LIST = URL_PERSONAL_DIARY + "/diarylist";

        public static final String  URL_DIARY_ENTRY_LIST = URL_PERSONAL_DIARY + "/diaryentrylist";

        public static final String  URL_USER_LIST = URL_PERSONAL_DIARY + "/userlist";

        public static final String  URL_ENTRY_EDITOR = URL_PERSONAL_DIARY + "/entryeditor";

        public static final String  URL_ERROR = URL_PERSONAL_DIARY + "/error";

        public static final String URL_AUTHORISE = URL_PERSONAL_DIARY + "/authorise";
    }
    public static class RolesName {
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
        public static final String PERMIT_ALL = "permitAll";
    }

    public enum DiaryEntryAction {
        ADD,
        DELETE,
        OPEN,
        EDIT
    }

    public static class SessionAtribute{
        public static final String USER_ID = "id";
        public static final String DIARY_ID = "diaryId";
        public static final String ENTRY_ACTION = "EntryAction";
        public static final String ENTRY_ID = "entryId";
        public static final String USER_NAME = "UserName";
        public static final String WARNING_MESSAGE = "WarningMessage";
        public static final String ERROR_MESSAGE = "ErrorMessage";
        public static final String EXCEPTION = "Exception";
        public static final String DIARY_NAME = "diaryName";
    }


}
