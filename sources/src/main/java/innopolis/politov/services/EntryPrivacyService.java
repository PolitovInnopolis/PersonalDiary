package innopolis.politov.services;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.converters.EntryPrivacyConverter;
import innopolis.politov.models.pojo.EntryPrivacy;
import innopolis.politov.models.repositories.EntryPrivacyRepository;
import innopolis.politov.services.eservice.EntryPrivacyServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by General on 3/12/2017.
 */
@Service
public class EntryPrivacyService implements IEntryPrivacyService {
    private static final Logger logger = Logger.getLogger(EntryTypeService.class);
    private EntryPrivacyRepository entryPrivacyRepository;

    @Override
    public List<EntryPrivacy> getAllEntryPrivacy() throws EntryPrivacyServiceException {
        try {
            return EntryPrivacyConverter.convert(entryPrivacyRepository.findByActive(ConstEntityActive
                    .ENTITY_ACTIVE));
        } catch (Exception e) {
            logger.error(e);
            throw new EntryPrivacyServiceException(e);
        }
    }

    @Autowired
    public void setEntryPrivacyRepository(EntryPrivacyRepository entryPrivacyRepository) {
        this.entryPrivacyRepository = entryPrivacyRepository;
    }
}
