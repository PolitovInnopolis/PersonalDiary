package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/11/2017.
 */
public class EntryTypeServiceException extends GeneralServiceException {
    public EntryTypeServiceException() {
    }

    public EntryTypeServiceException(String message) {
        super(message);
    }

    public EntryTypeServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntryTypeServiceException(Throwable cause) {
        super(cause);
    }

    public EntryTypeServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
