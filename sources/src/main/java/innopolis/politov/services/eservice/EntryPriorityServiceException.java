package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/11/2017.
 */
public class EntryPriorityServiceException extends GeneralServiceException {
    public EntryPriorityServiceException() {
    }

    public EntryPriorityServiceException(String message) {
        super(message);
    }

    public EntryPriorityServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntryPriorityServiceException(Throwable cause) {
        super(cause);
    }

    public EntryPriorityServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
