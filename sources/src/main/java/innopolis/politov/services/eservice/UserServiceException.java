package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/1/2017.
 */
public class UserServiceException extends GeneralServiceException {
    public UserServiceException() {
        super();
    }

    public UserServiceException(String message) {
        super(message);
    }

    public UserServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserServiceException(Throwable cause) {
        super(cause);
    }

    public UserServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
