package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/4/2017.
 */
public class DiaryServiceException extends GeneralServiceException {
    public static final String ERROR_MESSAGE = "DIARY SERVICE ERROR: ";

    public DiaryServiceException() {
        this("");
    }

    public DiaryServiceException(String message) {
        super(ERROR_MESSAGE +message);
    }

    public DiaryServiceException(String message, Throwable cause) {
        super(ERROR_MESSAGE + message, cause);
    }

    public DiaryServiceException(Throwable cause) {
        super(cause);
    }

    public DiaryServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
