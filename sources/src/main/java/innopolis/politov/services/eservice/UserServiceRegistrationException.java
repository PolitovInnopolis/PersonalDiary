package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/5/2017.
 */
public class UserServiceRegistrationException extends GeneralServiceException {
    public static final String ERROR_MESSAGE = "Введены некорректные данные для регистрации."
            + " Убедитесь, что вы заполнили все поля регистрации и повторите попытку. В случае " +
            "повторной ошибки сообщите администрации сайта. ";

    public UserServiceRegistrationException() {
        this("");
    }

    public UserServiceRegistrationException(String message) {
        super(ERROR_MESSAGE + message);
    }

    public UserServiceRegistrationException(String message, Throwable cause) {
        super(ERROR_MESSAGE + message, cause);
    }

    public UserServiceRegistrationException(Throwable cause) {
        this("", cause);
    }

    public UserServiceRegistrationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
