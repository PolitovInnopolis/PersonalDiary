package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/9/2017.
 */
public class DiaryEntryServiceException extends GeneralServiceException {
    public DiaryEntryServiceException() {
    }

    public DiaryEntryServiceException(String message) {
        super(message);
    }

    public DiaryEntryServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiaryEntryServiceException(Throwable cause) {
        super(cause);
    }

    public DiaryEntryServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
