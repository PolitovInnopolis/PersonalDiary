package innopolis.politov.services.eservice;

import innopolis.politov.services.GeneralServiceException;

/**
 * Created by General on 3/11/2017.
 */
public class EntryPrivacyServiceException extends GeneralServiceException {
    public EntryPrivacyServiceException() {
    }

    public EntryPrivacyServiceException(String message) {
        super(message);
    }

    public EntryPrivacyServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntryPrivacyServiceException(Throwable cause) {
        super(cause);
    }

    public EntryPrivacyServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
