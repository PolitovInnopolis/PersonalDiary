package innopolis.politov.services;

import innopolis.politov.models.pojo.DiaryEntry;
import innopolis.politov.services.eservice.DiaryEntryServiceException;

import java.util.List;

/**
 * Created by General on 3/4/2017.
 */
public interface IDiaryEntryService {
    List<DiaryEntry> getEntriesByDiaryId(Long diaryId) throws DiaryEntryServiceException;

    DiaryEntry getEntryById(Long entryId) throws DiaryEntryServiceException;

    void addNewEntry(Long diaryId, Long idType, Long idPriority, Long idPrivacy
            , String title, String tag, String text, String imageLink) throws
            DiaryEntryServiceException;

    void deleteEntryById(Long diaryId, Long entryId) throws DiaryEntryServiceException;

    void updateEntry(Long diaryId, Long entryId, Long idType, Long idPriority, Long idPrivacy,
                     String title, String tag, String text, String imageLink) throws DiaryEntryServiceException;
}
