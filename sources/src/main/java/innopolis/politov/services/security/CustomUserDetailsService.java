package innopolis.politov.services.security;


import innopolis.politov.models.pojo.User;
import innopolis.politov.services.IUserService;
import innopolis.politov.services.eservice.UserServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;

/**
 * Created by igor2i on 09.03.17.
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    public static final Logger logger = Logger.getLogger(CustomUserDetailsService.class);
    private final IUserService userService;

    @Autowired
    public CustomUserDetailsService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = null;
        try {
            user = userService.getUserByLogin(username);
        } catch (UserServiceException e) {
            logger.error(e);
            throw new UsernameNotFoundException("User not found");
        }

        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found");
        } else {
            logger.trace("User UnBlocked = " + user.getUnBlocked());
            List<GrantedAuthority> roles = new LinkedList<>();
            roles.add(new SimpleGrantedAuthority(user.getRole().getName()));

            CustomUserDetails customUserDetails = new CustomUserDetails(user, roles);
            return customUserDetails;
        }
    }

}
