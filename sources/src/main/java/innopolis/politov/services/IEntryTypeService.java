package innopolis.politov.services;

import innopolis.politov.models.pojo.EntryType;
import innopolis.politov.services.eservice.EntryTypeServiceException;

import java.util.List;

/**
 * Created by General on 3/11/2017.
 */
public interface IEntryTypeService {
    List<EntryType> getAllEntryType() throws EntryTypeServiceException;
}
