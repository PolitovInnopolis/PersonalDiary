package innopolis.politov.services;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.common.ConstPageParams;
import innopolis.politov.common.ExceptionUserServiceUserDataNotValid;
import innopolis.politov.common.UserDaoExceptionUserIsExists;
import innopolis.politov.models.converters.RoleConverter;
import innopolis.politov.models.converters.UserConverter;
import innopolis.politov.models.entity.RolesEntity;
import innopolis.politov.models.entity.UsersEntity;
import innopolis.politov.models.pojo.Role;
import innopolis.politov.models.pojo.User;
import innopolis.politov.models.repositories.RoleRepository;
import innopolis.politov.models.repositories.UserRepository;
import innopolis.politov.services.eservice.UserServiceException;
import innopolis.politov.services.eservice.UserServiceRegistrationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by General on 2/26/2017.
 */
@Service
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class UserService implements IUserService{
    private static Logger logger = Logger.getLogger(UserService.class);

    private static final byte USER_BLOCKED = 0;
    private static final byte USER_NO_BLOCKED = 1;

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserConverter userConverter;

    //todo: - заменить на JSR-307
    private void validateUserData(String email, String password, String
            firstName, String lastName, String birthday, String telephoneNumber) throws ExceptionUserServiceUserDataNotValid {
        if(!email.matches("[a-zA-Z0-9_]+@[a-zA-Z0-9]+\\.[a-zA-Z]+"))
            throw new ExceptionUserServiceUserDataNotValid("email = " + email);
//        if(!password.matches("[a-zA-Z0-9_.]+"))
//            throw new ExceptionUserServiceUserDataNotValid("password = " +password);
        if(!firstName.matches("[а-яА-Яa-zA-Z]+") )
            throw new ExceptionUserServiceUserDataNotValid("firstName = " + firstName);
        if(!lastName.matches("[а-яА-Яa-zA-Z]+"))
            throw new ExceptionUserServiceUserDataNotValid("lastName = " + lastName);
        if(!telephoneNumber.matches("\\+[0-9]{7,13}"))
            throw new ExceptionUserServiceUserDataNotValid("telephoneNumber = " + telephoneNumber);
        java.util.Date birthdayDate = null;
        try {
            birthdayDate = java.sql.Date.valueOf(birthday);
           if (birthdayDate == null || birthdayDate.getTime() > new Date().getTime())
               throw new ExceptionUserServiceUserDataNotValid("birthday =" + birthday.toString());
        }catch (IllegalArgumentException e){
            throw new ExceptionUserServiceUserDataNotValid("birthday =" + birthday.toString(), e);

        }
    }

    public  User authorize(String login, String password) throws UserServiceException{
        try {
            logger.trace("authorize");
            return userConverter.convert(userRepository.findByEmailAndPassword(login, password));
        } catch (Exception e) {
            logger.error(e);
            throw new UserServiceException(e);
        }
    }

    public Boolean registrationUser(String email, String password, String
            firstName, String lastName, String birthday, String telephoneNumber) throws UserServiceRegistrationException {
        try {
            validateUserData(email, password, firstName, lastName, birthday, telephoneNumber);
            Role role = RoleConverter.convert(roleRepository.findByName(ConstPageParams.RolesName
                    .ROLE_USER));
            UsersEntity usersEntity = new UsersEntity(
                    0,
                    firstName,
                    lastName,
                    java.sql.Date.valueOf(birthday),
                    email,
                    password,
                    telephoneNumber,
                    role.getId().intValue(),
                    USER_NO_BLOCKED,
                    0,
                    ConstEntityActive.ENTITY_ACTIVE);
            userRepository.save(usersEntity);
            return true;
        }
        catch (Exception e){
            logger.error(e);
            throw new UserServiceRegistrationException(e);
        }
    }

    public Boolean registrationAdmin(String email, String password, String
            firstName, String lastName, String birthday, String telephoneNumber) throws
            UserDaoExceptionUserIsExists, UserServiceException {
        try {
            validateUserData(email, password, firstName, lastName, birthday, telephoneNumber);
            Role role = RoleConverter.convert(roleRepository.findByName(ConstPageParams.RolesName
                    .ROLE_ADMIN));
            UsersEntity usersEntity = new UsersEntity(
                    0,
                    firstName,
                    lastName,
                    java.sql.Date.valueOf(birthday),
                    email,
                    password,
                    telephoneNumber,
                    role.getId().intValue(),
                    USER_NO_BLOCKED,
                    0,
                    ConstEntityActive.ENTITY_ACTIVE);
            userRepository.save(usersEntity);
            return true;
        }catch (Exception e){
            logger.error(e);
            throw new UserServiceException(e);
        }
    }

    public List<User> getAllUser() throws UserServiceException {
        try {
            logger.trace("getAllUser");
            return userConverter.convert(userRepository.findByActive((int)ConstEntityActive
                    .ENTITY_ACTIVE));
        } catch (Exception e) {
            logger.error(e);
            throw new UserServiceException(e);
        }
    }

    public Boolean isUserAdmin(Long userId) throws UserServiceException {
        try {
            UsersEntity usersEntity = userRepository.findOne(userId.intValue());
            RolesEntity rolesEntity = roleRepository.findOne(usersEntity.getRoleId());
            return rolesEntity.getName().equals(ConstPageParams.RolesName.ROLE_ADMIN);
        } catch (Exception e) {
            logger.error(e);
            throw new UserServiceException(e);
        }
    }

    @Override
    public void blockedUser(Long userId, Boolean unBlocked) throws UserServiceException {

        try {
            UsersEntity usersEntity = userRepository.findOne(userId.intValue());
            if (unBlocked){
                usersEntity.setBlocked(USER_BLOCKED);
            }else{
                usersEntity.setBlocked(USER_NO_BLOCKED);
            }
            userRepository.save(usersEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new UserServiceException(e);
        }
    }

    @Override
    public User getUserByLogin(String login) throws UserServiceException {
        try {
            logger.trace("getUserByLogin: " + login);
            UsersEntity usersEntity = userRepository.findByEmailAndActive(login,
                    (ConstEntityActive.ENTITY_ACTIVE));
            logger.trace("UserEntity: " + usersEntity);
            return  userConverter.convert(usersEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new UserServiceException(e);
        }
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Autowired
    public void setUserConverter(UserConverter userConverter) {
        this.userConverter = userConverter;
    }
}
