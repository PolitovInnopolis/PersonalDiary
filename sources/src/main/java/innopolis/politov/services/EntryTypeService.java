package innopolis.politov.services;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.converters.EntryTypeConverter;
import innopolis.politov.models.pojo.EntryType;
import innopolis.politov.models.repositories.EntryTypeRepository;
import innopolis.politov.services.eservice.EntryTypeServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by General on 3/11/2017.
 */
@Service
public class EntryTypeService implements IEntryTypeService {
    private static final Logger logger = Logger.getLogger(EntryTypeService.class);
    private EntryTypeRepository entryTypeDao;

    @Override
    public List<EntryType> getAllEntryType() throws EntryTypeServiceException {
        try {
            return EntryTypeConverter.convert(entryTypeDao.findByActive(ConstEntityActive
                    .ENTITY_ACTIVE));
        } catch (Exception e) {
            logger.error(e);
            throw new EntryTypeServiceException(e);
        }
    }

    @Autowired
    public void setEntryTypeDao(EntryTypeRepository entryTypeDao) {
        this.entryTypeDao = entryTypeDao;
    }
}
