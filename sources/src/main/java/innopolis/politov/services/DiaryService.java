package innopolis.politov.services;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.converters.DiaryConverter;
import innopolis.politov.models.entity.DiaryEntity;
import innopolis.politov.models.pojo.Diary;
import innopolis.politov.models.repositories.DiaryRepository;
import innopolis.politov.services.eservice.DiaryServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by General on 2/26/2017.
 */
@Service
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class DiaryService implements IDiaryService {
    public static final Logger logger = Logger.getLogger(DiaryService.class);

    private DiaryRepository diaryRepository;

    public DiaryService() {
    }

    public List<Diary> getDiariesByUserId(Long userId) throws DiaryServiceException {
        try {
            logger.trace("getDiariesByUserId begin");
            List<DiaryEntity> diaryEntities = diaryRepository.findByUserIdAndActive(userId
                            .intValue(), ConstEntityActive.ENTITY_ACTIVE);
            return DiaryConverter.convert(diaryEntities);
        }catch (Exception e){
            logger.error(e);
            throw new DiaryServiceException(e);
        }
    }

    public Boolean createNewDiary(Long userId, String diaryName) throws DiaryServiceException {
        try {
            DiaryEntity diaryEntity = new DiaryEntity(
                    0,
                    diaryName,
                    userId.intValue()
                    , 0,
                    (int)ConstEntityActive.ENTITY_ACTIVE);
            diaryRepository.save(diaryEntity);
            return true;
        } catch (Exception e) {
            logger.error(e);
            throw new DiaryServiceException(e);
        }
    }

    public Boolean deleteDiary(Long diaryId) throws DiaryServiceException {
        try {
            DiaryEntity diaryEntity = diaryRepository.findOne(diaryId.intValue());
            diaryEntity.setActive(((int) ConstEntityActive.ENTITY_NO_ACTIVE));
            diaryRepository.save(diaryEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new DiaryServiceException(e);
        }
        return true;
    }

    @Autowired
    public void setDiaryRepository(DiaryRepository diaryRepository) {
        this.diaryRepository = diaryRepository;
    }
}
