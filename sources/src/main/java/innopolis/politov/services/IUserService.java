package innopolis.politov.services;

import innopolis.politov.common.UserDaoExceptionUserIsExists;
import innopolis.politov.services.eservice.UserServiceException;
import innopolis.politov.models.pojo.User;
import innopolis.politov.services.eservice.UserServiceRegistrationException;

import java.util.List;

/**
 * Created by General on 3/2/2017.
 */
public interface IUserService {

    User authorize(String login, String password) throws UserServiceException;

    Boolean registrationUser(String email, String password, String
            firstName, String lastName, String birthday, String telephoneNumber) throws
            UserServiceRegistrationException;

    Boolean registrationAdmin(String email, String password, String
            firstName, String lastName, String birthday, String telephoneNumber) throws
            UserDaoExceptionUserIsExists, UserServiceException;

    List<User> getAllUser() throws UserServiceException;

    Boolean isUserAdmin(Long userId) throws UserServiceException;

    void blockedUser(Long userId, Boolean unBlocked) throws UserServiceException;


    User getUserByLogin(String login) throws UserServiceException;
}
