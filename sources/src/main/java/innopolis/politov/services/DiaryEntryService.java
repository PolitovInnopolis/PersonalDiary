package innopolis.politov.services;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.converters.DiaryEntryConverter;
import innopolis.politov.models.entity.DiaryEntryEntity;
import innopolis.politov.models.pojo.DiaryEntry;
import innopolis.politov.models.repositories.DiaryEntryRepository;
import innopolis.politov.services.eservice.DiaryEntryServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by General on 2/27/2017.
 */
@Service
public class DiaryEntryService implements IDiaryEntryService {
    public static final Logger logger = Logger.getLogger(DiaryService.class);

    private DiaryEntryRepository diaryEntryRepository;
    private DiaryEntryConverter diaryEntryConverter;

    @Override
    public List<DiaryEntry> getEntriesByDiaryId(Long diaryId) throws DiaryEntryServiceException {
        logger.trace("getEntriesByDiaryId");
        try {
            List<DiaryEntryEntity> entryEntities = diaryEntryRepository.findByDiaryIdAndActive(diaryId
                            .intValue(), ConstEntityActive.ENTITY_ACTIVE);
            return diaryEntryConverter.convert(entryEntities);
        } catch (Exception e) {
           logger.error(e);
           throw new DiaryEntryServiceException(e);
        }
    }

    @Override
    public DiaryEntry getEntryById(Long entryId) throws DiaryEntryServiceException {
        logger.trace("getEntryById");
        try {
            DiaryEntryEntity entryEntity = diaryEntryRepository.findByIdAndActive(
                    entryId.intValue(), ConstEntityActive.ENTITY_ACTIVE);
            return diaryEntryConverter.convert(entryEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new DiaryEntryServiceException(e);
        }
    }

    @Override
    public void deleteEntryById(Long diaryId, Long entryId) throws DiaryEntryServiceException {
        try {
            logger.trace("deleteEntryById");
            DiaryEntryEntity diaryEntryEntity = diaryEntryRepository.findOne(entryId.intValue());
            diaryEntryEntity.setActive(( ConstEntityActive.ENTITY_NO_ACTIVE));
            diaryEntryRepository.save(diaryEntryEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new DiaryEntryServiceException();
        }
    }

    @Override
    public void updateEntry(Long diaryId, Long entryId, Long idType, Long idPriority, Long idPrivacy
            , String title, String tag, String text, String imageLink) throws DiaryEntryServiceException {
        try {
            logger.trace("updateEntry");
            DiaryEntryEntity diaryEntryEntity = diaryEntryRepository.findOne(entryId.intValue());
            diaryEntryEntity.setTypeId(idType.intValue());
            diaryEntryEntity.setPriorityId(idPriority.intValue());
            diaryEntryEntity.setPrivacyId(idPrivacy.intValue());
            diaryEntryEntity.setTitle(title);
            diaryEntryEntity.setText(text);
            diaryEntryEntity.setImageLink(imageLink);
            diaryEntryEntity.setDateTime(Timestamp.valueOf(LocalDateTime.now()));
            diaryEntryRepository.save(diaryEntryEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new DiaryEntryServiceException();
        }
    }

    @Override
    public void addNewEntry(Long diaryId, Long idType, Long idPriority, Long idPrivacy
            , String title, String tag, String text, String imageLink) throws DiaryEntryServiceException {
        logger.trace("addNewEntry");
        try {
            DiaryEntryEntity diaryEntity = new DiaryEntryEntity(
                    0,
                    diaryId.intValue(),
                    idType.intValue(),
                    idPriority.intValue(),
                    idPrivacy.intValue(),
                    title,text,
                    tag,
                    Timestamp.valueOf(LocalDateTime.now()),
                    imageLink,
                    0,
                    ConstEntityActive.ENTITY_ACTIVE);
            diaryEntryRepository.save(diaryEntity);
        } catch (Exception e) {
            logger.error(e);
            throw new DiaryEntryServiceException(e);
        }
    }

    @Autowired
    public void setDiaryEntryRepository(DiaryEntryRepository diaryEntryRepository) {
        this.diaryEntryRepository = diaryEntryRepository;
    }

    @Autowired
    public void setDiaryEntryConverter(DiaryEntryConverter diaryEntryConverter) {
        this.diaryEntryConverter = diaryEntryConverter;
    }
}
