package innopolis.politov.services;

import innopolis.politov.models.pojo.Diary;
import innopolis.politov.services.eservice.DiaryServiceException;

import java.util.List;

/**
 * Created by General on 3/3/2017.
 */
public interface IDiaryService {
    List<Diary> getDiariesByUserId(Long userId) throws DiaryServiceException;

    Boolean createNewDiary(Long userId, String diaryName) throws DiaryServiceException;

    Boolean deleteDiary(Long diaryId) throws DiaryServiceException;

}