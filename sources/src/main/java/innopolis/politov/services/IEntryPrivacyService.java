package innopolis.politov.services;

import innopolis.politov.models.pojo.EntryPrivacy;
import innopolis.politov.services.eservice.EntryPrivacyServiceException;

import java.util.List;

/**
 * Created by General on 3/11/2017.
 */
public interface IEntryPrivacyService {
    List<EntryPrivacy> getAllEntryPrivacy() throws EntryPrivacyServiceException;
}
