package innopolis.politov.services;

import innopolis.politov.models.pojo.EntryPriority;
import innopolis.politov.services.eservice.EntryPriorityServiceException;

import java.util.List;

/**
 * Created by General on 3/11/2017.
 */
public interface IEntryPriorityService {
    List<EntryPriority> getAllEntryPriority() throws EntryPriorityServiceException;
}
