package innopolis.politov.services;

import innopolis.politov.common.ConstEntityActive;
import innopolis.politov.models.converters.EntryPriorityConverter;
import innopolis.politov.models.pojo.EntryPriority;
import innopolis.politov.models.repositories.EntryPriorityRepository;
import innopolis.politov.services.eservice.EntryPriorityServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by General on 3/11/2017.
 */
@Service
public class EntryPriorityService implements IEntryPriorityService {
    public static final Logger logger = Logger.getLogger(EntryPriorityService.class);
    private EntryPriorityRepository entryPriorityDao;

    @Override
    public List<EntryPriority> getAllEntryPriority() throws EntryPriorityServiceException {
        try{
            return EntryPriorityConverter.convert(entryPriorityDao.findByActive(ConstEntityActive
                    .ENTITY_ACTIVE));
        }catch (Exception e){
            logger.error(e);
            throw new EntryPriorityServiceException(e);
        }
    }

    @Autowired
    public void setEntryPriorityDao(EntryPriorityRepository entryPriorityDao) {
        this.entryPriorityDao = entryPriorityDao;
    }
}
