package innopolis.politov.controllers;

import innopolis.politov.common.LoginSessionExcepion;
import innopolis.politov.common.ServletDiaryIsNullException;
import innopolis.politov.common.ServletFeatureNotFoundException;
import innopolis.politov.common.UnknownServletActionException;
import innopolis.politov.controllers.eservlet.ServletActionException;
import innopolis.politov.common.ConstPageParams.*;
import innopolis.politov.models.pojo.Diary;
import innopolis.politov.services.IDiaryService;
import innopolis.politov.services.IUserService;
import innopolis.politov.services.eservice.DiaryServiceException;
import innopolis.politov.services.eservice.UserServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by General on 2/26/2017.
 */
@Controller
public class DiaryListController {

    enum ActionDiaryServlet {
        Delete, Open, Edit, Add
    }

    private static Logger logger = Logger.getLogger(DiaryListController.class);

    public static final String REQUEST_PARAM_DIARY_ID = "diaryId";
    public static final String REQUEST_PARAM_DIARY_ACTION = "DiaryAction";
    public static final String REQUEST_PARAM_DIARY_NAME = "newDiary";




    private IDiaryService diaryService;

    /**
     *  Задает представление для отображения результата по выполнению действия
     * @param session - текущая сессия
     * @param diaryId - id дневника над которым выполняется действие
     * @param actionDiaryServlet - действие над дневником
     * @param params - дополнительные параметры для действий
     * @throws LoginSessionExcepion
     * @throws ServletFeatureNotFoundException
     * @throws ServletActionException
     */
    private String runActionAndSGetNextUrl(
            HttpSession session
            , Long diaryId
            , ActionDiaryServlet actionDiaryServlet
            , String ... params

    ) throws LoginSessionExcepion, ServletFeatureNotFoundException, ServletActionException {
        Long userId = SessionAttributeController.getSessionId(session);
        try {
            switch (actionDiaryServlet) {
                case Delete: {
                    diaryService.deleteDiary(diaryId);
                    return Urls.URL_DIARY_LIST;
                }
                case Edit: {
                    throw new ServletFeatureNotFoundException();
                }
                case Open: {
                    return Urls.URL_DIARY_ENTRY_LIST;
                }
                case Add: {
                    if (params.length > 0 && !params[0].equals(null) && !params[0].isEmpty()) {
                        logger.trace(" Add new diary "+ params[0]);
                        diaryService.createNewDiary(userId, params[ 0 ]);
                    }else {
                        SessionAttributeController.setSessionWarningMessage(
                                session, "Название дневника не должно быть пустым");
                    }
                    return Urls.URL_DIARY_LIST;
                }
                default:
                    throw new ServletFeatureNotFoundException();
            }
        }catch (DiaryServiceException e) {
            logger.error(e);
            throw new ServletActionException(e);
        }

    }



    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @GetMapping(value = {Urls.URL_DIARY_LIST})
    public ModelAndView doGet(HttpSession session){
        logger.trace("Get");
        ModelAndView modelAndView = new ModelAndView(Views.ERROR);
        try {
            Long userId = SessionAttributeController.getSessionId(session);
            logger.trace("User ID = " + userId);
            List<Diary> diaries = diaryService.getDiariesByUserId(userId);
            logger.debug("Diary count = " + diaries.size());
            modelAndView.addObject("diaries", diaries);
            modelAndView.setViewName(Views.DIARY_LIST);
        }
        catch (Exception e){
            logger.error(e);
            SessionAttributeController.setExceptionAndSetView(modelAndView, e);
        }
        return modelAndView;
    }

    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @PostMapping(value = {Urls.URL_DIARY_LIST})
    public String doPost(
            @RequestParam(name = REQUEST_PARAM_DIARY_ID , required = false)Long diaryId,
            @RequestParam(name = REQUEST_PARAM_DIARY_ACTION )ActionDiaryServlet actionDiaryServlet,
            @RequestParam(name =REQUEST_PARAM_DIARY_NAME, defaultValue = "")String diaryName,
            HttpSession session
    ) {
        logger.trace("post");
        String url = null;
        try {
            session.removeAttribute(SessionAtribute.WARNING_MESSAGE);
            if (diaryId!=null) {
                session.setAttribute(SessionAtribute.DIARY_ID, diaryId);
                logger.debug("Diary ID = " + diaryId);
            }
            session.setAttribute(SessionAtribute.DIARY_NAME, diaryName);
            url = runActionAndSGetNextUrl(session, diaryId
                    , actionDiaryServlet, diaryName);
        }
        catch (ServletFeatureNotFoundException | LoginSessionExcepion | ServletActionException e){
            logger.error(e);
            url = SessionAttributeController.setExceptionAndGetUrl(session, e);
        }
        return Urls.REDIRECT_TO + url;
    }

    @Autowired
    public void setDiaryService(IDiaryService diaryService) {
        this.diaryService = diaryService;
    }

}
