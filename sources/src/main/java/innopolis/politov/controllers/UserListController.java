package innopolis.politov.controllers;

import innopolis.politov.common.ExceptionNotAccessRights;
import innopolis.politov.common.LoginSessionExcepion;
import innopolis.politov.common.ServletFeatureNotFoundException;
import innopolis.politov.controllers.eservlet.RequestParamNotFound;
import innopolis.politov.models.pojo.User;
import innopolis.politov.services.IUserService;
import innopolis.politov.services.eservice.UserServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static innopolis.politov.common.ConstPageParams.*;

/**
 * Created by General on 2/28/2017.
 */
@Controller
public class UserListController  {
    private static Logger logger = Logger.getLogger(UserListController.class);

    enum ActionUserServlet {
        Block, UnBlock
    }

    public static final String REQUEST_PARAM_ACTION_USER = "UserAction";
    public static final String REQUEST_PARAM_USER_ID = "userId";

    private IUserService userService;

    private ActionUserServlet getActionUser(String actionUser) throws RequestParamNotFound{

        logger.trace("Action user = " + actionUser);
        if (actionUser == null)
            throw  new RequestParamNotFound();
        try {
            ActionUserServlet actionUserServlet = ActionUserServlet.valueOf(actionUser);
            return actionUserServlet;
        } catch (IllegalArgumentException e) {
            logger.error("Error in  the action user format" + actionUser, e);
            throw new RequestParamNotFound();
        }
    }

    private String runActionAndGetUrl(ActionUserServlet actionUserServlet, Long
            userId) throws ServletFeatureNotFoundException, UserServiceException {

        switch (actionUserServlet) {
            case Block: {
                userService.blockedUser(userId, true);
                return Urls.URL_USER_LIST;
            }
            case UnBlock: {
                userService.blockedUser(userId, false);
                return Urls.URL_USER_LIST;
            }
            default:
                throw new ServletFeatureNotFoundException();
        }


    }

    private Long getParametrUserId(String paramUserId) throws RequestParamNotFound {

        Long userId = null;
        if (paramUserId == null)
            throw new RequestParamNotFound();
         try {
             userId = Long.valueOf(paramUserId);
             logger.trace("User ID = " + userId);
             if (userId == null || userId < 1) {
                 throw new RequestParamNotFound();
             }
         }catch (NumberFormatException e){
             throw new RequestParamNotFound();
         }
        return userId;
    }


    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Secured({RolesName.ROLE_ADMIN})
    @GetMapping(value = {Urls.URL_USER_LIST})
    public ModelAndView doGet(HttpSession session) {
        logger.trace("Get");
        ModelAndView modelAndView = new ModelAndView(Views.ERROR);
        try {
            Long sessionId = SessionAttributeController.getSessionId(session);
            logger.trace("User session ID = " + sessionId);
            logger.trace("Is user admin = " + userService.isUserAdmin(sessionId));
            if (!userService.isUserAdmin(sessionId)){
                throw new ExceptionNotAccessRights();
            }
            List<User> users = userService.getAllUser();

            modelAndView.addObject("users", users);
            modelAndView.setViewName(Views.USER_LIST);

        } catch (LoginSessionExcepion | ExceptionNotAccessRights | UserServiceException e) {
            logger.error(e);
            SessionAttributeController.setExceptionAndSetView(modelAndView, e);
        }
        return modelAndView;
    }

    @Secured({RolesName.ROLE_ADMIN})
    @PostMapping(value = {Urls.URL_USER_LIST})
    public String doPost(
            @RequestParam(name = REQUEST_PARAM_ACTION_USER )ActionUserServlet actionUserServlet,
            @RequestParam(name = REQUEST_PARAM_USER_ID )Long userId,
            HttpSession session) {
        logger.trace("POST");
        String url = Urls.URL_ERROR;
        try {
            logger.trace("USER ACTION =" + actionUserServlet.name());
            url = runActionAndGetUrl(actionUserServlet, userId);

        } catch (ServletFeatureNotFoundException |  UserServiceException e) {
            logger.error(e);
            url = SessionAttributeController.setExceptionAndGetUrl(session, e);
        }
        return Urls.REDIRECT_TO + url;
    }
}
