package innopolis.politov.controllers;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;

import static innopolis.politov.common.ConstPageParams.*;

/**
 * Created by General on 3/6/2017.
 */
@Controller
public class LoginController {
    private static Logger logger = Logger.getLogger(LoginController.class);

    @Secured({RolesName.PERMIT_ALL})
    @GetMapping( value = {"/personaldiary/login", "/", "/personaldiary", "login"})
    public String showBasePage(
            @RequestParam(name = "WarningMessage", required = false) String warningMessage,
            Model model
    ) {
        logger.trace("GET");
        model.addAttribute("WarningMessage", warningMessage);
        return Views.LOGIN;
    }
}
