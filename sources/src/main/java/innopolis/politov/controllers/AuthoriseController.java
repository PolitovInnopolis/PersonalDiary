package innopolis.politov.controllers;

import innopolis.politov.common.ConstPageParams;
import innopolis.politov.models.pojo.User;
import innopolis.politov.services.IUserService;
import innopolis.politov.services.security.CustomUserDetails;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by General on 3/15/2017.
 */
@Controller
public class AuthoriseController {
    private static Logger logger = Logger.getLogger(AuthoriseController.class);

    private IUserService userService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping(value = { ConstPageParams.Urls.URL_AUTHORISE})
    public String doGet(HttpSession session){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        String login = userDetails.getUsername();
        String password = userDetails.getUsername();
        logger.trace("GET");
        try {
            logger.trace("User name : " + login);
            logger.trace("User password : " + password);

            User user = userService.getUserByLogin(login);
            if (user != null) {
                session.setAttribute(ConstPageParams.SessionAtribute.USER_ID, user.getId());
                session.setAttribute(ConstPageParams.SessionAtribute.USER_NAME, user.getFirstName());
                logger.trace("User " + login + " autorized");
                logger.trace("User is " + user.getRole().getName());

                if ( !user.getRole().isAdmin()) {
                    return ConstPageParams.Urls.REDIRECT_TO + ConstPageParams.Urls.URL_DIARY_LIST;
                }else {
                    return ConstPageParams.Urls.REDIRECT_TO + ConstPageParams.Urls.URL_USER_LIST;
                }
            } else {
                SessionAttributeController.setSessionWarningMessage(session, "Логин или пароль неверны");
                logger.trace("User " + login + " not autorized");
                return ConstPageParams.Urls.REDIRECT_TO + ConstPageParams.Urls.URL_LOGIN;
            }
        } catch (Exception e) {
            logger.error(e);
            return  ConstPageParams.Urls.REDIRECT_TO + SessionAttributeController.setExceptionAndGetUrl(session, e);
        }

    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
