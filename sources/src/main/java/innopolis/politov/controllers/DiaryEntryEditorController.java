package innopolis.politov.controllers;

import innopolis.politov.models.pojo.DiaryEntry;
import innopolis.politov.models.pojo.EntryType;
import innopolis.politov.services.*;
import innopolis.politov.services.eservice.DiaryEntryServiceException;
import innopolis.politov.services.eservice.EntryTypeServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

import static innopolis.politov.common.ConstPageParams.*;


/**
 * Created by General on 3/9/2017.
 */
@Controller
public class DiaryEntryEditorController {
    private static Logger logger = Logger.getLogger(DiaryEntryEditorController.class);

    private IDiaryService diaryService;
    private IDiaryEntryService diaryEntryService;
    private IEntryPriorityService entryPriorityService;
    private IEntryTypeService entryTypeService;
    private IEntryPrivacyService entryPrivacyService;

    public static final String MODEL_PARAM_ENTRY_PRIORITIES = "entryPriorities";
    public static final String MODEL_PARAM_ENTRY_TYPES = "entryTypes";
    public static final String MODEL_PARAM_ENTRY_PRIVACIES = "entryPrivacies";
    public static final String MODEL_PARAM_ENTRY_ACTION = "entryAction";
    public static final String MODEL_STRING_ACTION = "stringAction";
    public static final String MODEL_IS_READONLY_ELEMENT = "isReadonly";
    public static final String MODEL_READONLY_ELEMENT = "readonly";
    public static final String MODEL_CURRENT_ENTRY_TYPE = "currentEntryTypeName";
    public static final String MODEL_CURRENT_ENTRY_TYPE_ID = "typeId";

    private void setEntryTypeForView(ModelAndView modelAndView) throws EntryTypeServiceException {
        List<EntryType> entryTypes = entryTypeService.getAllEntryType();
        if(entryTypes.size()>=1) {
            modelAndView.addObject(MODEL_CURRENT_ENTRY_TYPE, entryTypes.get(
                    entryTypes.size() - 1).getName());
            modelAndView.addObject(MODEL_CURRENT_ENTRY_TYPE_ID, entryTypes.get(
                    entryTypes.size() - 1).getId());
        }
    }

    private void prepareModel(HttpSession session, ModelAndView modelAndView) throws
            GeneralServiceException {

        String paramAction = session.getAttribute(SessionAtribute.ENTRY_ACTION).toString();
        DiaryEntryAction diaryEntryAction = DiaryEntryAction.valueOf(paramAction);
        DiaryEntry diaryEntry = new DiaryEntry();

        modelAndView.addObject(MODEL_PARAM_ENTRY_PRIORITIES, entryPriorityService.getAllEntryPriority());
        modelAndView.addObject(MODEL_PARAM_ENTRY_TYPES, entryTypeService.getAllEntryType());
        modelAndView.addObject(MODEL_PARAM_ENTRY_PRIVACIES, entryPrivacyService.getAllEntryPrivacy());
        modelAndView.addObject(MODEL_PARAM_ENTRY_ACTION, diaryEntryAction.name());

        logger.trace("Entry action " + diaryEntryAction.name());

        if (diaryEntryAction != DiaryEntryAction.ADD) {
            logger.trace("Get entry ID ");
            Long entryId = Long.valueOf(session.getAttribute(SessionAtribute.ENTRY_ID).toString());
            logger.trace("Entry ID " + entryId);
            diaryEntry = diaryEntryService.getEntryById(entryId);
        }
        switch (diaryEntryAction) {
            case ADD:

                modelAndView.addObject(MODEL_STRING_ACTION, "Добавление новой записи");
                setEntryTypeForView(modelAndView);
                modelAndView.setViewName(Views.DIARY_ENTRY_EDITOR);
                break;
            case OPEN: {
                modelAndView.setViewName(Views.DIARY_ENTRY_VIEWER);
                modelAndView.addObject(MODEL_STRING_ACTION, "Просмотр записи");
                logger.trace("Open entry");
            }
            break;
            case EDIT:
                setEntryTypeForView(modelAndView);
                modelAndView.addObject(MODEL_STRING_ACTION, "Редактирование записи");
                modelAndView.setViewName(Views.DIARY_ENTRY_EDITOR);
                break;
        }
        modelAndView.addObject("entry", diaryEntry);
    }

    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @GetMapping(value = { Urls.URL_ENTRY_EDITOR })
    public ModelAndView doGet(HttpSession session) {
        logger.trace("GET");
        ModelAndView modelAndView = new ModelAndView(Views.ERROR);
        try {
            prepareModel(session, modelAndView);
        } catch (Exception e) {
            logger.error(e);
            SessionAttributeController.setExceptionAndSetView(modelAndView, e);
        }
        return modelAndView;
    }

    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @PostMapping(value = { Urls.URL_ENTRY_EDITOR })
    public String doPost(//todo model attribute
            @RequestParam(name = "entryId", required = false) Long entryId,
            @RequestParam(name = "idType", required = false) Long idType,
            @RequestParam(name = "priority", required = false) Long idPriority,
            @RequestParam(name = "privacy", required = false) Long idPrivacy,
            @RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "tag", required = false) String tag,
            @RequestParam(name = "text", required = false) String text,
            @RequestParam(name = "imageLink", required = false) String imageLink,
            @RequestParam(name = "action", required = true) DiaryEntryAction entryAction,
            HttpSession session, HttpServletRequest request)  {
        SessionAttributeController.showRequestParams(request);
        logger.trace("POST");
        Long diaryId = Long.valueOf(session.getAttribute(SessionAtribute.DIARY_ID).toString());
        String url = Urls.URL_ERROR;
        try {
            switch (entryAction) {
                case ADD:
                    diaryEntryService.addNewEntry(diaryId, idType, idPriority, idPrivacy, title, tag, text,
                            imageLink);
                    url = Urls.URL_DIARY_ENTRY_LIST;
                    break;
                case EDIT:
                    diaryEntryService.updateEntry(diaryId, entryId, idType, idPriority, idPrivacy,
                            title, tag, text,
                            imageLink);
                    url = Urls.URL_DIARY_ENTRY_LIST;
                    break;
                case OPEN:
                    url = Urls.URL_DIARY_ENTRY_LIST;
                    break;
            }
        }
        catch (DiaryEntryServiceException e){
            logger.error(e);
            url = SessionAttributeController.setExceptionAndGetUrl(session, e);
            //todo exception resolver

        }

        return Urls.REDIRECT_TO + url;
    }


    @Autowired
    public void setDiaryService(IDiaryEntryService diaryEntryService) {
        this.diaryEntryService = diaryEntryService;
    }

    @Autowired
    public void setEntryPriorityService(IEntryPriorityService entryPriorityService) {
        this.entryPriorityService = entryPriorityService;
    }

    @Autowired
    public void setEntryTypeService(IEntryTypeService entryTypeService) {
        this.entryTypeService = entryTypeService;
    }

    @Autowired
    public void setEntryPrivacyService(IEntryPrivacyService entryPrivacyService) {
        this.entryPrivacyService = entryPrivacyService;
    }

    @Autowired
    public void setDiaryService(IDiaryService diaryService) {
        this.diaryService = diaryService;
    }

    @Autowired
    public void setDiaryEntryService(IDiaryEntryService diaryEntryService) {
        this.diaryEntryService = diaryEntryService;
    }
}
