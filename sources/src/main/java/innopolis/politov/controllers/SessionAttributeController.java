package innopolis.politov.controllers;

import innopolis.politov.common.ConstPageParams;
import innopolis.politov.common.LoginSessionExcepion;
import innopolis.politov.controllers.eservlet.SessionParamNotFound;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

import static innopolis.politov.common.ConstPageParams.*;

/**
 * Created by General on 2/26/2017.
 */
public class SessionAttributeController {

    private static Logger logger = Logger.getLogger(SessionAttributeController.class);

    /**
     * Получает id сессии(пользователя)
     * @param session - текущая сессия
     * @return - возвращает id сессии(пользователя)
     * @throws LoginSessionExcepion - если сессия не создана или не задан id сессии
     */
    public static Long getSessionId(HttpSession session)throws LoginSessionExcepion {
        if (session == null)
            throw new LoginSessionExcepion();

        Enumeration attributeNames =  session.getAttributeNames();
        if (attributeNames == null)
            throw new LoginSessionExcepion();

        String attribute = session.getAttribute(ConstPageParams.SessionAtribute.USER_ID).toString();
        Long atributeValue = null;
        if (attribute != null) {
            try {
                atributeValue =  Long.valueOf(attribute);
            } catch (NumberFormatException e) {
                throw new LoginSessionExcepion("Session id not corrected", e);
            }
        }
        return atributeValue;

    }

    public static void closeUserSession(HttpSession session){
        session.removeAttribute(SessionAtribute.USER_ID);
        session.invalidate();
    }

    public static void setSessionWarningMessage(HttpSession session, String warningMessage) {
        session.setAttribute(SessionAtribute.WARNING_MESSAGE, warningMessage);
    }

    public static void clearSessionWarningMessage(HttpSession session){
        session.removeAttribute(SessionAtribute.WARNING_MESSAGE);
    }

    /**
     * Добавляет в сессию исключение, возникшее в результате работы
     * @param exc - возникщее исключение
     */
    public static String setExceptionAndGetUrl(HttpSession session, Exception exc) {
        session.setAttribute(SessionAtribute.EXCEPTION, exc);
        return Urls.URL_ERROR;

    }

    public static void setExceptionAndSetView(ModelAndView modelAndView, Exception exc){
        modelAndView.addObject(SessionAtribute.EXCEPTION, exc);
        modelAndView.setViewName(Views.ERROR);
    }

    /**
     * Возвращает параметр - ID дневника, находящегося в HTTP сессии
     * @param session -HTTP сессии
     * @return
     */
    public static Long getDiaryId(HttpSession session) throws SessionParamNotFound {
        if (session == null)
            throw new SessionParamNotFound();

        Enumeration attributeNames =  session.getAttributeNames();
        if (attributeNames == null)
            throw new SessionParamNotFound();

        String attribute = session.getAttribute(SessionAtribute.DIARY_ID).toString();
        Long attributeValue = null;
        if (attribute != null) {
            try {
                attributeValue =  Long.valueOf(attribute);
            } catch (NumberFormatException e) {
                throw new SessionParamNotFound("Session param is not corrected", e);
            }
        }
        return attributeValue;
    }

    public static String getSessionErrorMessage(HttpSession session) throws SessionParamNotFound {
        if (session == null)
            throw new SessionParamNotFound();

        Enumeration attributeNames =  session.getAttributeNames();
        if (attributeNames == null)
            throw new SessionParamNotFound();

        Object attribute = session.getAttribute(ConstPageParams.SessionAtribute.EXCEPTION);
        logger.trace("Exception in session = " + attribute);
        String errorMessage = null;
        if (attribute != null){
            Exception exception = (Exception)attribute;
            logger.error(exception);
            errorMessage = exception.getMessage();
//            session.setAttribute(ConstPageParams.SessionAtribute.EXCEPTION, exception.getMessage());
            session.removeAttribute(ConstPageParams.SessionAtribute.EXCEPTION);
        }
        return errorMessage;
    }


    public static void showRequestParams(HttpServletRequest request){
        Enumeration enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()){
            String paramName = enumeration.nextElement().toString();
            logger.debug("{ " + paramName + ":" + request.getParameter(paramName) +" }");
        }

        enumeration = request.getSession().getAttributeNames();
        while (enumeration.hasMoreElements()){
            String paramName = enumeration.nextElement().toString();
            logger.debug("< " + paramName + ":" + request.getParameter(paramName) +" >");
        }

    }
}
