package innopolis.politov.controllers;

import innopolis.politov.common.ServletFeatureNotFoundException;
import innopolis.politov.controllers.eservlet.RequestParamNotFound;
import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import static innopolis.politov.common.ConstPageParams.*;

/**
 * Created by General on 3/1/2017.
 */
@Secured({RolesName.PERMIT_ALL})
@Controller
public class MenuController extends HttpServlet {
    private static Logger logger = Logger.getLogger(MenuController.class);

    enum ActionMenuServlet {
        Logout
    }

    public static final String REQUEST_PARAM_MENU_ACTION = "MenuAction";

    private ActionMenuServlet getActionMenu(String menuAction) throws RequestParamNotFound {
        logger.trace(menuAction);
        if (menuAction == null) {
            throw new RequestParamNotFound();
        }
        try {
            ActionMenuServlet actionMenuServlet = ActionMenuServlet.valueOf(menuAction);
            return actionMenuServlet;
        } catch (IllegalArgumentException e) {
            logger.error(e);
            throw new RequestParamNotFound();
        }
    }

    private String runActionAndGetUrl(ActionMenuServlet actionMenuServlet, HttpSession session)
            throws ServletFeatureNotFoundException{
        switch (actionMenuServlet) {
            case Logout: {
                SessionAttributeController.closeUserSession(session);
                return Urls.URL_LOGIN;
            }
            default:
                throw new ServletFeatureNotFoundException();
        }

    }

    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @GetMapping( value = {"/personaldiary/menu", "menu", "/menu.jsp", "menu.jsp"})
    public ModelAndView doGet(){
        logger.trace(" GET");
        return  new ModelAndView(Views.LOGIN);
    }

    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @PostMapping( value = {"/personaldiary/menu", "menu", "/menu.jsp", "menu.jsp"})
    public String doPost(
            HttpSession session,
            SessionStatus sessionStatus,
            @RequestParam(name = REQUEST_PARAM_MENU_ACTION , defaultValue = "")String menuAction
    ){
        logger.trace("POST");
        String url = Urls.URL_ERROR;
        try {
            ActionMenuServlet actionMenuServlet = getActionMenu(menuAction);
            url = runActionAndGetUrl(actionMenuServlet, session);
            sessionStatus.setComplete();

        } catch (ServletFeatureNotFoundException | RequestParamNotFound  e) {
            logger.error(e);
            SessionAttributeController.setExceptionAndGetUrl(session, e);
        }
        return Urls.REDIRECT_TO +  url;
    }
}
