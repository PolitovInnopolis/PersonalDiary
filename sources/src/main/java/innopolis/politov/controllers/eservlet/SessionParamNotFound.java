package innopolis.politov.controllers.eservlet;

/**
 * Created by General on 3/8/2017.
 */
public class SessionParamNotFound extends Exception {
    public static final String ERROR_MESSAGE = "Паоаметр сессии не найден. ";

    public SessionParamNotFound() {
        this("");
    }

    public SessionParamNotFound(String message) {
        super(ERROR_MESSAGE + message);
    }

    public SessionParamNotFound(String message, Throwable cause) {
        super(ERROR_MESSAGE + message, cause);
    }

    public SessionParamNotFound(Throwable cause) {
        super(cause);
    }

    public SessionParamNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(ERROR_MESSAGE + message, cause, enableSuppression, writableStackTrace);
    }
}
