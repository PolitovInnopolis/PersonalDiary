package innopolis.politov.controllers.eservlet;

/**
 * Created by General on 3/4/2017.
 */
public class ServletActionException extends Exception {
    public ServletActionException() {
    }

    public ServletActionException(String message) {
        super(message);
    }

    public ServletActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServletActionException(Throwable cause) {
        super(cause);
    }

    public ServletActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
