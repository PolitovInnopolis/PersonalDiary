package innopolis.politov.controllers.eservlet;

/**
 * Created by General on 3/8/2017.
 */
public class RequestParamNotFound extends Exception {
    public static final String ERROR_MESSAGE = "Параметр запроса не найден или некорректен. ";

    public RequestParamNotFound() {
        this("");
    }

    public RequestParamNotFound(String message) {
        super(ERROR_MESSAGE + message);
    }

    public RequestParamNotFound(String message, Throwable cause) {
        super(ERROR_MESSAGE + message, cause);
    }

    public RequestParamNotFound(Throwable cause) {
        super(cause);
    }

    public RequestParamNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(ERROR_MESSAGE + message, cause, enableSuppression, writableStackTrace);
    }
}
