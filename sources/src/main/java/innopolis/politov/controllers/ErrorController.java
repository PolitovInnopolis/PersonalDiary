package innopolis.politov.controllers;

import innopolis.politov.common.ConstPageParams;
import innopolis.politov.controllers.eservlet.SessionParamNotFound;
import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by General on 3/6/2017.
 */
@Controller
public class ErrorController {
    Logger logger = Logger.getLogger(ErrorController.class);

    @Secured({ ConstPageParams.RolesName.PERMIT_ALL})
    @GetMapping( value = {"/personaldiary/error", "/error", "error", "error.jsp"})
    public ModelAndView doGet(HttpSession session) throws SessionParamNotFound {
        logger.trace("GET");
        ModelAndView modelAndView = new ModelAndView(ConstPageParams.Views.ERROR);
        try {
            modelAndView.addObject(ConstPageParams.SessionAtribute.ERROR_MESSAGE,
                    SessionAttributeController.getSessionErrorMessage(session));
            session.removeAttribute(ConstPageParams.SessionAtribute.ERROR_MESSAGE);
        } catch (SessionParamNotFound e) {
            logger.error(e);
            throw e;
        }
        return modelAndView;
    }

    @Secured({ ConstPageParams.RolesName.PERMIT_ALL})
    @PostMapping( value = {"/personaldiary/error", "/error", "error", "error.jsp"})
    public void doPost() {

    }
}
