package innopolis.politov.controllers;

import innopolis.politov.common.ServletFeatureNotFoundException;
import innopolis.politov.controllers.eservlet.ServletActionException;
import innopolis.politov.models.pojo.DiaryEntry;
import innopolis.politov.services.IDiaryEntryService;
import innopolis.politov.services.IDiaryService;
import innopolis.politov.services.eservice.DiaryEntryServiceException;
import innopolis.politov.services.eservice.DiaryServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static innopolis.politov.common.ConstPageParams.*;

/**
 * Created by General on 2/27/2017.
 */
@Controller
public class DiaryEntryListController {
    private static Logger logger = Logger.getLogger(DiaryEntryListController.class);

    public static final String REQUEST_PARAM_DIARY_ID = "diaryId";
    public static final String REQUEST_PARAM_ENTRY_ID = "entryId";
    public static final String REQUEST_PARAM_ENTRY_ACTION = "EntryAction";


    private IDiaryEntryService diaryEntryService;
    private IDiaryService diaryService;

    private String runActionAndSGetNextUrl(
            HttpSession session
            , Long diaryId
            , Long entryId
            , DiaryEntryAction diaryEntryAction

    ) throws ServletFeatureNotFoundException, ServletActionException, DiaryServiceException, DiaryEntryServiceException {
        switch (diaryEntryAction) {
            case ADD:{
                session.setAttribute(SessionAtribute.ENTRY_ACTION, diaryEntryAction.name());
                return Urls.URL_ENTRY_EDITOR;
            }
            case DELETE:{
                diaryEntryService.deleteEntryById(diaryId, entryId);
                return Urls.URL_DIARY_ENTRY_LIST;
            }
            case OPEN:{
                session.setAttribute(SessionAtribute.ENTRY_ID , entryId);
                session.setAttribute(SessionAtribute.ENTRY_ACTION, diaryEntryAction.name());
                return Urls.URL_ENTRY_EDITOR;
            }
            case EDIT: {
                session.setAttribute(SessionAtribute.ENTRY_ID, entryId);
                session.setAttribute(SessionAtribute.ENTRY_ACTION, diaryEntryAction.name());
                return Urls.URL_ENTRY_EDITOR;
            }
            default:
                throw new ServletFeatureNotFoundException();
        }
    }


    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @GetMapping(value = { Urls.URL_DIARY_ENTRY_LIST})
    public ModelAndView doGet(
            HttpSession session) {
        logger.trace("Get");
        ModelAndView modelAndView = new ModelAndView(Views.ERROR);
        try {
            Long diaryId = SessionAttributeController.getDiaryId(session);
            logger.debug( "Diary ID = " + diaryId);
            if (diaryId != null){
                List<DiaryEntry> entries = diaryEntryService.getEntriesByDiaryId(diaryId);
                modelAndView.addObject("entries", entries);
                logger.debug("Entry count = " + entries.size());
                modelAndView.setViewName(Views.DIARY_ENTRY_LIST);
            }
        }
        catch (Exception e){
            logger.error(e);
            SessionAttributeController.setExceptionAndSetView(modelAndView, e);
        }
        return modelAndView;
    }

    @Secured({RolesName.ROLE_ADMIN, RolesName.ROLE_USER})
    @PostMapping(value = { Urls.URL_DIARY_ENTRY_LIST})
    public String doPost(
            @RequestParam(name = REQUEST_PARAM_ENTRY_ACTION, required = true)DiaryEntryAction
                    diaryEntryAction,
            @RequestParam(name = REQUEST_PARAM_ENTRY_ID, required = false)Long entryId,
            HttpSession session){

        String url = Urls.URL_ERROR;
        try {
            logger.trace(diaryEntryAction);
            Long diaryId = Long.valueOf(session.getAttribute(SessionAtribute.DIARY_ID).toString());
            logger.trace("Entry ID = " + entryId);
            url = runActionAndSGetNextUrl(session, diaryId, entryId, diaryEntryAction);
        } catch (ServletActionException | DiaryServiceException |
                ServletFeatureNotFoundException | DiaryEntryServiceException e) {
            logger.error(e);
            url = SessionAttributeController.setExceptionAndGetUrl(session, e);
        }

        return Urls.REDIRECT_TO + url;
    }

    @Autowired
    public void setDiaryEntryService(IDiaryEntryService diaryEntryService) {
        this.diaryEntryService = diaryEntryService;
    }

    @Autowired
    public void setDiaryService(IDiaryService diaryService) {
        this.diaryService = diaryService;
    }
}
