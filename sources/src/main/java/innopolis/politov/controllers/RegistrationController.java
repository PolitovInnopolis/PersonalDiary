package innopolis.politov.controllers;

import innopolis.politov.services.IUserService;
import innopolis.politov.services.eservice.UserServiceRegistrationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import innopolis.politov.common.ConstPageParams.*;

import javax.servlet.http.HttpSession;


/**
 * Created by General on 3/6/2017.
 */
@Controller
public class RegistrationController {
    private static Logger logger = Logger.getLogger(RegistrationController.class);

    private BCryptPasswordEncoder bcryptEncoder;
    private IUserService userService;

    private static final String REQUEST_PARAM_EMAIL = "email";
    private static final String REQUEST_PARAM_LOGIN = "email";
    private static final String REQUEST_PARAM_PASSWORD = "password";
    private static final String REQUEST_PARAM_BIRTHDAY = "birthday";
    private static final String REQUEST_FIRST_NAME = "first_name";
    private static final String REQUEST_LAST_NAME = "last_name";
    private static final String REQUEST_TELEPHONE_NUMBER = "telephone_number";

    @GetMapping( value = {"/personaldiary/registration"})
    public ModelAndView doGet() {
        logger.trace("GET");
        ModelAndView modelAndView = new ModelAndView(Views.REGISTRATION);
        return modelAndView;
    }

    @PostMapping(value = {"/personaldiary/registration"})
    public String doPost(
            @RequestParam(name = REQUEST_PARAM_LOGIN , defaultValue = "")String email,
            @RequestParam(name = REQUEST_PARAM_PASSWORD , defaultValue = "")String password,
            @RequestParam(name = REQUEST_FIRST_NAME , defaultValue = "")String firstName,
            @RequestParam(name = REQUEST_LAST_NAME , defaultValue = "")String lastName,
            @RequestParam(name = REQUEST_PARAM_BIRTHDAY , defaultValue = "")String birthday,
            @RequestParam(name = REQUEST_TELEPHONE_NUMBER , defaultValue = "")String
                    telephoneNumber,
            HttpSession session
    ) {
        logger.trace("POST");
        logger.debug("firstName = " + firstName);
        String url = null;
        try {
            password = bcryptEncoder.encode(password);
            userService.registrationUser(email, password, firstName, lastName, birthday, telephoneNumber);
            url = Urls.URL_LOGIN;
        } catch (UserServiceRegistrationException e) {
            logger.error(e);
            url = SessionAttributeController.setExceptionAndGetUrl(session, e);
        }
        return Urls.REDIRECT_TO + url;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setBcryptEncoder(BCryptPasswordEncoder bcryptEncoder) {
        this.bcryptEncoder = bcryptEncoder;
    }
}
