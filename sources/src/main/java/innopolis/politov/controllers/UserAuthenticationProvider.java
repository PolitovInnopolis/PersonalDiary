package innopolis.politov.controllers;

import innopolis.politov.services.security.CustomUserDetails;
import innopolis.politov.services.security.CustomUserDetailsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


/**
 * Created by General on 3/16/2017.
 */
public final class UserAuthenticationProvider implements AuthenticationProvider {

    private static Logger logger = Logger.getLogger(UserAuthenticationProvider.class);

    private CustomUserDetailsService userDetailsService;

    private BCryptPasswordEncoder bcryptEncoder;

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        logger.trace("Authenticate " +
                "started............................................................");
        String password = authentication.getCredentials().toString();
        String userName = authentication.getName();
        CustomUserDetails userDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(userName);
        logger.trace("USER DETAIL PASSWORD = " + userDetails.getPassword());
        logger.trace("USER DETAIL ENCODE PASSWORD = " + bcryptEncoder.encode(password));
        if (userDetails == null) {
            logger.error("User not found. UserName=" + userName);
            throw new BadCredentialsException(userName);
        } else if (!userDetails.isEnabled()) {
            logger.error("Not enabled.");
            throw new DisabledException(userName);
        } else {
            if (bcryptEncoder.matches(password, userDetails.getPassword())) {
                logger.trace("Authenticate " +
                        "started............................................................");
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                logger.info("authenticate finished.");
                return token;
            } else {
                logger.error("Password does not match. UserName=" + userName);
                throw new BadCredentialsException(userName);
            }

        }
    }

    @Override
    public boolean supports(Class<? extends Object> authentication) {
        return UsernamePasswordAuthenticationToken.class
                .isAssignableFrom(authentication);
    }

    @Autowired
    public void setUserService(CustomUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setBcryptEncoder(BCryptPasswordEncoder bcryptEncoder) {
        this.bcryptEncoder = bcryptEncoder;
    }

}