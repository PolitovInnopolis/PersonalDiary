package innopolis.politov;


import innopolis.politov.models.database.DataBaseManager;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.sql.SQLException;

public class Main {
    public static final Logger logger = Logger.getLogger(Main.class);
    static
    {
        BasicConfigurator.configure();
    }

    public static void main(String[] args) throws SQLException {
            DataBaseManager.ExportDataBaseToXml();
            DataBaseManager.ClearAllTableInDb();
            DataBaseManager.ImportDataBaseFromXml();
    }


    }




