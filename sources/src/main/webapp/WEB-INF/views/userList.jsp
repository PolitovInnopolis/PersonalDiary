<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: General
  Date: 2/28/2017
  Time: 11:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список пользователей</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
<%@include file="menu.jsp"%>
<h1>Список пользователей</h1>
<table class="table" border="1" width="100%" cellpadding="5">
    <tr class="table-row-cell">
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Роль</th>
        <th>Email</th>
    </tr>
    <jsp:useBean id="users" scope="request"
                 type="java.util.List<innopolis.politov.models.pojo.User>"/>
    <c:forEach items="${users}" var="user">
        <form action="/personaldiary/userlist" method="post">
            <tr class="table-row-cell">
                <input type = "hidden" name="userId" value=<c:out
                        value="${user.getId()}" /> />
                <td class="table-cell">
                    <c:out value="${user.firstName}"></c:out>
                </td>
                <td class="table-cell">
                    <c:out value="${user.lastName}"></c:out>
                </td>
                <td class="table-cell">
                    <c:out value="${user.role.name}"></c:out>
                </td>
                <td class="table-cell">
                    <c:out value="${user.email}"></c:out>
                </td>
                <td class="table-cell">
                    <c:if test="${!user.unBlocked}">
                        <button class="btn btn-success" type="submit" name="UserAction" value="UnBlock"
                                formmethod="post" >
                            Разблокировать
                        </button>
                    </c:if>
                    <c:if test="${user.unBlocked}">
                        <button class="btn btn-danger" type="submit" name="UserAction"
                                value="Block"formmethod="post" >
                            Заблокировать
                        </button>
                    </c:if>
                </td>
            </tr>
        </form>
    </c:forEach>
</table>

</body>
</html>
