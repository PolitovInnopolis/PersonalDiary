<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: bot
  Date: 23.02.17
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"  language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/buttons_span.css"/>/>
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/style.css"/> />
    <link href="<c:url value="/resources/css/signin.css" />" rel="stylesheet">
</head>
<body>
    <div class="container" align="center">
        <form action="/personaldiary/registration" method="post">
            <h2 class="form-signin-heading">Регистрация</h2>
            <p><label for="email">email:</label><br>
            <input class="form-control" type="email" name="email" id="email" value="" required style="width: 35%"
                   placeholder="Input" pattern="[a-zA-Z0-9_]+@[a-zA-Z0-9]+\.[a-zA-Z]+"><br></p>

            <p><label for="password">Пароль:</label><br>
            <input class="form-control" type="password" name="password" id="password" value=""
                   placeholder="Input" style="width: 35%"
                  required pattern="[a-zA-Z0-9]{4,16}"><br></p>

            <p><label for="first_name">Имя:</label><br>
            <input class="form-control"  type="text" name="first_name" id="first_name" value=""
                   required style="width: 35%"
                   placeholder="Input" pattern="[а-яА-Яa-zA-Z]+"><br></p>

            <p><label for="last_name">Фамилия:</label><br>
            <input class="form-control"  type="text" name="last_name" id="last_name" value=""
                   required style="width: 35%"
                   placeholder="Input" pattern="[а-яА-Яa-zA-Z]+"><br></p>

            <p><label for="birthday">Дата рождения:</label><br>
            <input class="form-control" type="date" name="birthday" id="birthday"
                   value="2015-10-09" required style="width: 35%"
                   placeholder="Input"><br></p>

            <p><label for="telephone_number">Телефон:</label><br>
            <input  class="form-control" type="tel" name="telephone_number" id="telephone_number"
                    value="" required style="width: 35%"
                   placeholder="+7 xxx xx" pattern="\+[0-9]{7,13}}"><br></p>

            <p><button class="btn btn-lg btn-primary btn-block" type="submit"  formmethod="post" style="width: 35%">
               Зарегистрироваться
            </button><br></p>
        </form>
        <h1><c:out value="${WarningMessage}"></c:out></h1>
    </div>
</body>
</html>
