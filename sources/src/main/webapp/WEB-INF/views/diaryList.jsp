<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: General
  Date: 2/26/2017
  Time: 3:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%--@elvariable id="diaries" type="java.util.List<innopolis.politov.models.pojo.Diary>"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<html>
<head>
    <title>Список дневников</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="css/entryTypeImage.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/buttons_span.css"/>/>
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/style.css"/> />
</head>
<body>
    <%@include file="menu.jsp"%>
    <h1>Ваш список дневников</h1>
    <table border="1" width="100%" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>


        <c:forEach items="${diaries}" var="diary">
            <form action="/personaldiary/diarylist" method="post">
                <tr>
                    <input type = "hidden" name="diaryId" value=<c:out
                            value="${diary.getId()}" /> />
                    <td>
                        <c:out value="${diary.getId()}"></c:out>
                    </td>
                    <td>
                        <c:out value="${diary.getName()}"></c:out>
                    </td>
                    <td>
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="DiaryAction" value="Open"
                                formmethod="post" > Открыть </button>
                    </td>

                    <td>
                        <button class="btn btn-danger btn-primary btn-block" type="submit" name="DiaryAction" value="Delete"
                                formmethod="post" >
                            Удалить
                        </button>
                    </td>
                </tr>
            </form>
        </c:forEach>
        <tr>
            <form action="/personaldiary/diarylist" method="post">
                <td>
                    <h1>*</h1>
                </td>
                <td>
                    <input class="form-control" type="text"  name="newDiary" id="newDiary"
                           value="" placeholder="Имя дневника">
                </td>
                <td>
                    <button class="btn btn-success" type="submit" name="DiaryAction" value="Add"
                            formmethod="post" >
                        Добавить
                    </button>
                </td>
            </form>
        </tr>
    </table>
<h1><c:out value="${WarningMessage}"></c:out> </h1>
</body>
</html>
