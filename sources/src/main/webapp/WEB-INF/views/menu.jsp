<%--
  Created by IntelliJ IDEA.
  User: General
  Date: 3/1/2017
  Time: 9:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Личный кабинет</title>
    <link href="css/entryTypeImage.css" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/buttons_span.css"/>/>
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/style.css"/> />
</head>
<body>
<div>
    <table align="right" border="0"  cellpadding="2">
        <tr>
            <td align="right">
                <h2><c:out value="${UserName}"></c:out></h2>
            </td>
            <td align="right">
                <form action="/personaldiary/menu" method="post">
                    <button class="btn btn-danger" type="submit" name="MenuAction"  value="Logout"
                            formmethod="post">
                        Выход
                    </button>
                </form>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
