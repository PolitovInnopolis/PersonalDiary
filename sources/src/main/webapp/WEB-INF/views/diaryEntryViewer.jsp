<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: General
  Date: 3/9/2017
  Time: 5:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Просмотр записи</title>
    <link href="css/entryTypeImage.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/buttons_span.css"/>/>
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/style.css"/> />

    <link href="<c:url value="/resources/css/entryTypeImage.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/signin.css" />" rel="stylesheet">

    <script type="text/javascript"
            src=<c:url value="/resources/js/modernizr.custom.79639.js"/>"js/modernizr.custom.79639.js"></script>
    <noscript><link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/noJS.css"/>/></noscript>

</head>
<body>
<jsp:useBean id="entryPriorities" scope="request"
             type="java.util.List<innopolis.politov.models.pojo.EntryPriority>"/>
<jsp:useBean id="entryTypes" scope="request"
             type="java.util.List<innopolis.politov.models.pojo.EntryType>"/>
<%--@elvariable id="entry" type="innopolis.politov.models.pojo.DiaryEntry"--%>
        <form action="/personaldiary/entryeditor" method="post">
            <div class="container">

                <input type="hidden" name="entryId" value="${entry.id}">

                <input type="text" class="form-control"  readonly
                       value="${entry.entryType.name}" style="width: 100%">

                <input type="text" class="form-control"  readonly
                       value="${entry.tag}" style="width: 100%">

                <input type="text" class="form-control"  readonly
                       value="${entry.title}" style="width: 100%">

                <input type="text" class="form-control"  readonly
                       value="${entry.entryPrivacy.name}" style="width: 100%">

                <input type="text" class="form-control"  readonly
                       value="${entry.dateTime}" style="width: 100%">
                <br>

                <input type="text" class="form-control"  readonly
                       value="${entry.text}" style="width: 100%">
                <br>
                <img class="photocard" src=" ${entry.imageLink}">


                <input type="hidden" name="action" readonly id="action"
                       value="${entryAction}"></input>

                <button class="btn btn-lg btn-primary btn-block" type="submit"
                        name="ok" id="ok" value="OK}"
                        formmethod="post" style="width: 100%">
                    Вернуться к просмотру списка записей
                </button><br>
            </div>

        </form>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">

    function DropDown(el) {
        this.dd = el;
        this.placeholder = this.dd.children('input');
        this.opts = this.dd.find('ul.dropdown > li');
        this.val = '';
        this.index = -1;
        this.initEvents();
    }
    DropDown.prototype = {
        initEvents : function() {
            var obj = this;

            obj.dd.on('click', function(event){
                $(this).toggleClass('active');
                return false;
            });

            obj.opts.on('click',function(){
                var opt = $(this);
                obj.val = opt.text();
                obj.index = opt.index();
                var t = document.querySelector("#entryType1");
                t.setAttribute("value", opt.text().trim());
                var tId = document.querySelector("#idType");
                tId.setAttribute("value", opt.attr("id"));
                obj.placeholder.text(obj.val);
            });
        },
        getValue : function() {
            return this.val;
        },
        getIndex : function() {
            return this.index;
        }
    }

    $(function() {

        var dd = new DropDown( $('#dd') );

        $(document).click(function() {
            // all dropdowns
            $('.wrapper-dropdown-3').removeClass('active');
        });

    });

</script>


</body>
</html>
