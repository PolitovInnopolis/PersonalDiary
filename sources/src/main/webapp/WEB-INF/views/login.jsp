<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Login</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/signin.css" />" rel="stylesheet">
    </head>
    <body>
        <div align="center" class="container">

            <spring:url var="authUrl" value="/personaldiary/login" />
            <form class="form-signin" role="form" action="${authUrl}" method="post">

                <h2 class="form-signin-heading">Пожалуйста авторизуйтесь</h2>

                <input type="text" class="form-control" placeholder="Email address"
                       required autofocus name="j_username" id="j_username" value="" style="width: 35%"><br>

                <input type="password" class="form-control" name="j_password"
                       id="j_password" value="" placeholder="Password" required
                       style="width: 35%"><br>

                <label class="checkbox" style="width: 35%">
                    <input type="checkbox" value="remember-me"> Remember me
                </label><br><br>

                <button class="btn btn-lg btn-primary btn-block" type="submit"  formmethod="post" style="width: 35%">
                    Войти
                </button><br>

                <a class="btn btn-default" href="/personaldiary/registration" style="width: 35%">Регистрация</a>

                <h1><c:out value="${WarningMessage}"></c:out></h1>
            </form>
        </div>

    </body>
</html>
