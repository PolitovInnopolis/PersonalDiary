<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: General
  Date: 2/26/2017
  Time: 3:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>DiaryEntries</title>
    <link href="css/entryTypeImage.css" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/buttons_span.css"/>/>
    <link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/style.css"/> />
</head>
<%@include file="menu.jsp"%>
<body>
    <h1 align="center">Записи в дневнике ${diaryName}</h1>
    <table  border="1" width="100%">
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Tag</th>
            <th>Текст</th>
            <th>Приватность</th>
            <th>Приоритет</th>
            <th>Дата и время создания</th>
            <th>Изображение</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <c:forEach items="${entries}" var="entry">
            <form action="/personaldiary/diaryentrylist" method="post">
                <tr class="table-row-cell">
                    <td class="table-cell"><input type="text" readonly style="border: 0"
                                                  name="entryId" value=${entry.id}>
                    </td>
                    <td class="table-cell"><c:out value="${entry.getTitle()}"></c:out></td>
                    <td class="table-cell"> <c:out value="${entry.getTag()}"></c:out></td>
                    <td class="table-cell"><c:out value="${entry.getText()}"></c:out></td>
                    <td class="table-cell"><c:out value="${entry.getEntryPrivacy().getName()}"></c:out></td>
                    <td class="table-cell"><c:out value="${entry.getEntryPriority().getName()}"></c:out></td>
                    <td class="table-cell"><c:out value="${entry.getDateTime()}"></c:out></td>
                    <td class="table-cell"> <image class="photocard" src="${entry.getImageLink()}" /></td>
                    <td class="table-cell">
                        <button class="btn btn-default" type="submit" name="EntryAction" value="OPEN"
                                formmethod="post">
                            Открыть запись</button> </td>
                    <td class="table-cell">
                        <button class="btn btn-lg" type="submit" name="EntryAction" value="EDIT"
                                formmethod="post">
                            Редактировать запись</button> </td>
                    <td class="table-cell"> <button class="btn btn-danger" type="submit"
                                              name="EntryAction" value="DELETE" formmethod="post">Удалить
                        запись</button> </td>
                </tr>
            </form>
        </c:forEach>
    </table>
    <form action="/personaldiary/diaryentrylist" method="post">
        <button  class="btn btn-success" type="submit" name="EntryAction" style="align-self: flex-end"
                 value="ADD"
                formmethod="post">Добавить
            запись</button>
    </form>

</body>
</html>
